#ifndef TACTICAL_COMBAT_WIDGET_EVENT_HANDLER_H
#define TACTICAL_COMBAT_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "PlayerManager.h"

// TacticalCombatWidgetEventHandler is a widget handler specifically made for the 
// TacticalCombat states GUI, it's essentially for attaching to the Window and buttons
// that are created in that state. It then gets looped through continually by the widgetManager
// and checks for any left click events. If any occur the code in handleMouseLeftClick is executed.
// This event handlers specific job is to check for button presses on the on screen "Unit Cards" 
// in the combat map and then change the players currently selected target to the Unit that button
// represents! It does this by holding a pointer pointing to the current PlayerManager class and 
// holding a vector of all the Players units.
class TacticalCombatWidgetEventHandler: public WidgetEventHandler
{
public: 
	TacticalCombatWidgetEventHandler(PlayerManager * pManager, std::vector<Unit*> pUnits) {playerManager = pManager; playerUnits = pUnits;}
	void handleMouseLeftClick(Widget *);

private:
	std::vector<Unit*> playerUnits;
	PlayerManager * playerManager;
};

#endif