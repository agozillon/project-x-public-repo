#include "UnitData.h"
#include <sstream>
#include <fstream>

#include <iostream>
std::map<std::string,UnitData> UnitData::units;


UnitData::UnitData()
{

}



UnitData::UnitData(const std::string & n, const std::map<std::string,Attribute> at):
	name(n),attributes(at)
{
}


const Attribute & UnitData::getAttribute(const std::string & n) const
{
	std::map<std::string,Attribute>::const_iterator itr = attributes.find(n);

	if( itr == attributes.end() ) 
		return Attribute("NULL",dtSTRING);

	return itr->second;
	

}

const UnitData & UnitData::getUnit(const std::string & name)
{	
	std::map<std::string,UnitData>::iterator itr = units.find(name);

	if( itr == units.end() ) 
		return units["NULL"];

	return itr->second;
}



void UnitData::load(const std::string & filePath)
{
	std::ifstream file(filePath,std::ios::binary | std::ios::beg | std::ios::in); //load file in 
	std::stringstream stream;
	stream << file.rdbuf();  //and put the contents into a stream




	for(std::string str = ""; stream.good(); stream >> str) 
	{		
		if( str.compare("") == 0) continue; //skip over first dud line

		if( str[0] == '/' && str[1] == '/' ) //if commented, ignore that line
		{
			char line[256];
			stream.getline(line,256);
			continue;
		}

		
		std::string next;
		stream >>  next;

	
		if( next[0] != '{' )
			continue;

		while( str.find("_") != -1 ) //remove the underscores and replace with spaces
		{
			str.replace(str.find("_"),1," ");
		}

		std::map<std::string,Attribute> attributes;

		for(std::string str = ""; str.compare("") == 0 || str[0] != '}'; stream >> str)
		{
			if( str.compare("") ==0 ) continue;

			std::string attrib,value;
			

			if( str[0] == '/' && str[1] == '/' ) //if commented, ignore that line
			{
				char line[256];
				stream.getline(line,256);
				continue;
			}

			stream >>  attrib >> value;

			if(      str.compare("String")  == 0) attributes[attrib] = Attribute(value,dtSTRING);
			else if( str.compare("Integer") == 0) attributes[attrib] = Attribute(value,dtINTEGER);
			else if( str.compare("Float")   == 0) attributes[attrib] = Attribute(value,dtFLOAT);
			else if( str.compare("Boolean") == 0) attributes[attrib] = Attribute(value,dtBOOL);
		}


		units[str] = UnitData(str,attributes);

	}
}