#include "Particles.h"
#include <ctime>
#include <cstdlib>
#include <iostream>
#include <stack>
#include "CameraManager.h"
#include "GameManager.h"
#include "ShaderManager.h"
#include "TextureManager.h"

// Constructor that sets up the initial positions of the particles randomly within the specified box area!
Particles::Particles(const int numOfParticles, const glm::vec3 emitterDimensions, const glm::vec3 emitterPosition, const glm::vec2 screenResolution, const float size, std::string texture, std::string shader)
	: numParticles(numOfParticles), centerPosition(emitterPosition), dimensions(emitterDimensions), screenSize(screenResolution), particleSize(size), textureName(texture), shaderName(shader) 
{
	if(numParticles <= 0)
		return;

	positions = new GLfloat[numParticles * 3];
	velocity = new GLfloat[numParticles * 3];

	// randomizing the positions of all the particles and randomizing there Z velocity, they only 
	// ever move upwards never left or right
	for(int i = 0; i < numParticles * 3; i+=3){
		positions[i] = (std::rand() / ((float)RAND_MAX / (dimensions.x * 2)) - dimensions.x);
		positions[i+1] = (std::rand() / ((float)RAND_MAX / (dimensions.y * 2)) - dimensions.y);
		positions[i+2] = centerPosition.z;

		velocity[i] = 0;
		velocity[i+1] = 0;
		velocity[i+2] = (std::rand() % 100) / 10000.0f;
	}

	init(); // call init to set up the VAO
}

// VAO setup, all it needs is vertex data for each point right now however the VAO
// allows us to add more complex data to it if we wish, colour, velocity, fade etc
void Particles::init()
{
	glGenVertexArrays(1, &vaoID);
	glBindVertexArray(vaoID);

	glGenBuffers(1, &vboID); // generate the buffers
	glBindBuffer(GL_ARRAY_BUFFER, vboID); // bind vbo
	glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * (numParticles * 3), positions, GL_DYNAMIC_DRAW); // setup GL buffer
	glEnableVertexAttribArray(0);   // specify the Attrib its linked to in the shader
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0); // link to buffer
		
}

void Particles::update()
{
	// incrementing all the positions by the velocity of the particles. 
	for(int i = 0; i < numParticles * 3; i+=3){
		positions[i] += velocity[i];
		positions[i+1] += velocity[i+1];
		positions[i+2] += velocity[i+2];

		if(positions[i+2] >= centerPosition.z + dimensions.z)
		{
			positions[i+2] = centerPosition.z;
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, vboID);
	glBufferData(GL_ARRAY_BUFFER,  sizeof(GLfloat) * (numParticles * 3), NULL, GL_STREAM_DRAW); // clearing buffer
	glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(GLfloat) * (numParticles * 3), positions); //subbing new data in
 
}

// particles are the only draw method that takes an uncombined view and projection as it's required
// for the shader! This is the one exception however so we just pull the view and projection from the 
// CameraManager, instead of passing and combining the view and projection for everything else
// essentially saves a fair bit of CPU time as we only need to do the view projection combination
// once for every other draw (instead of 10-20 times) 
void Particles::draw(mat4 view, mat4 projection)
{
	std::stack<mat4> mStack;

	// draw object
	mStack.push(mat4(1.0));
	mStack.top() = translate(mStack.top(), centerPosition);

	// combining the viewProjection(Projection * view) and the model matrix to make the MVP(projection * view * model) 
	mat4 temp = view * mStack.top();
	ShaderManager::getInstance()->use(shaderName.c_str());
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("P", 1, false, value_ptr(projection));  //set the Projection
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniformMatrix4fv("MV", 1, false, value_ptr(temp));  //set the Projection
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniform1f("particleSize", particleSize); // particle point size
	ShaderManager::getInstance()->getShader(shaderName.c_str())->setUniform2fv("screenSize", 1, value_ptr(screenSize)); // the screens height and width
	TextureManager::getInstance()->bind(textureName.c_str()); // Find and bind that texture

	// binding the VAO and rendering the particles
	glBindVertexArray(vaoID);
	glDrawArrays(GL_POINTS, 0, numParticles);
	glBindVertexArray(0);
}

Particles::~Particles()
{
	delete []positions;
	delete []velocity;
}