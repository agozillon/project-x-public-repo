#ifndef AUDIO_H
#define AUDIO_H
#include <string>
#include "bass.h"

// Audio class that holds a Bass audio sample and invokes various Bass library
// functions on it to manipulate it, such as resuiming, playing and pausing 
// also capable of looping the track and changing the volume of it. 
class Audio
{
public:
	Audio(const char* fileName, bool looped);
	void play();
	void resume();
	void pause();
	void updateVolume(int volume);

private:
	HSAMPLE sound;
	HCHANNEL channel;
};

#endif