#include "Sprite.h"
#include "Mesh.h"

Sprite::Sprite()
{
	texture = "NULL";
	initGL();
}


/*
	This creates (if not created already) a blank square for the texture to occupy 
*/
void Sprite::initGL()
{

	if( RenderManager::getInstance()->contains("BlankMesh") == false)
	{
		// we will need to do somthing about these hard coded values later
		float vert[18]; 
		vert[0] =0.0; vert[1] = 1.0; vert[2] = 0.0;
		vert[3] =0.0; vert[4] =0.0; vert[5] = 0.0;
		vert[6] = 1.0; vert[7] =0.0; vert[8] = 0.0;

		vert[9]  =0.0; vert[10] = 1.0; vert[11] = 0.0;
		vert[12] = 1.0; vert[13] = 1.0; vert[14] = 0.0;
		vert[15] = 1.0; vert[16] =0.0; vert[17] = 0.0;

		float uv[12];

		uv[0] =  1.0f; uv[1] = 0.0f;
		uv[2] =  1.0f; uv[3] = 1.0f;
		uv[4] =  0.0f; uv[5] = 1.0f;


		uv[6] = 1.0f; uv[7] = 0.0f;
		uv[8] = 0.0f; uv[9] = 0.0f;
		uv[10]= 0.0f; uv[11] = 1.0f;

	
		RenderManager::getInstance()->addRenderable("BlankMesh",new Mesh(vert,18,uv,12)); 

	}

}


Sprite::~Sprite()
{

}


Sprite::Sprite(const std::string& textureName)
{
	texture = textureName;
	initGL();
}

void Sprite::render() const
{
	TextureManager::getInstance()->bind(texture);
	RenderManager::getInstance()->renderRenderable("BlankMesh");
}
