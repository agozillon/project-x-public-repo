#ifndef WIDGET_H
#define WIDGET_H

#include <Map>
#include <Queue>
#include "WidgetEventHandler.h"
#include "WidgetUpdateHandler.h"
#include <string>


enum Flags
{
	WIDGET_NULL = 0x0,
	WIDGET_DRAGABLE = 0x1,
	WIDGET_RESIZEABLE=0x2,
	WIDGET_PERFORM_CHILD_CLEANUP = 0x4, //whether or not to delete children upon deconstruction
};

enum StatusFlags //these flags are set by the widget upon user interaction
{
	STATUS_NULL    = 0x0,
	STATUS_LEFT_CLICKED = 0x1,
	STATUS_RIGHT_CLICKED= 0x2,
	STATUS_DRAGGED = 0x8,
	STATUS_MOUSE_OVER= 0x10,
	STATUS_MOUSE_LEFT_RELEASE = 0x20,
	STATUS_MOUSE_RIGHT_RELEASE= 0x40,
	MOUSE_ONLY_STATUS_FLAG = 0xFF,//First 8 bits reserved for mouse interaction, this flag is a bitwise pattern for removing/only keeping the mouse flags

	STATUS_DOCKED = 0x100,
};


/*
 *Base class for all UI Widgets
*/
class Widget
{
public:

	Widget(const std::string &name, float ix,const float iy,const float width,const float height,unsigned int flags,Widget * parent = nullptr);	
	virtual ~Widget();

	virtual void draw() const = 0; 
	virtual void update(){if(updateHandler != NULL) updateHandler->update(this);}


	//Make a widget become a child of this
	virtual void addWidget(Widget * w);

	//Treats the child widget's coordinates as if they are in the parent's 'coord space'
	//-1,-1 being parent's x,y   1,1 being x+width,y+height
	virtual void addWidgetRelative(Widget *w);

	//Returns the child with that name
	virtual Widget * getWidget(const std::string &) const;

	//Immediatly remove and clean up memory
	virtual void removeWidget(const std::string & name);

	//Removes the widget at the end of checking mouse <-- much safer
	virtual void queueRemoveWidget(const std::string & name) const;
	
	//returns the parent of this widget
	inline Widget * getParent() const {return parent; }
	//sets the parent of this widget (assumes already a child of said parent)
	inline void setParent(Widget * p) { parent = p;}

	inline const float getX()             const {return x; }
	inline const float getY()             const {return y; }
	inline const float getWidth()		  const {return width;}
	inline const float getHeight()		  const {return height;}
	inline const float getOriginalWidth() const {return originalWidth;}
	inline const float getOriginalHeight()const {return originalHeight;}
	inline const float getXScale()        const { return width/originalWidth; }
	inline const float getYScale()        const { return height/originalHeight;}

	inline const std::string & getName()  const {return name;}

	inline WidgetEventHandler * getEventHandler()const { return eventHandler; }
	inline WidgetUpdateHandler * getUpdateHandler() const { return updateHandler;}

	//Returns the mask used to hold the status of the widget
	inline const unsigned int getStatus() const{ return status;}

	inline void setStatus(const unsigned int newStatus) { status = newStatus; }


	inline const unsigned int getFlags() const { return flags;}
	inline void setFlags(const unsigned int newFlags) { flags = newFlags; }

	//returns true if an interaction takes place between the mouse and the Widget
	virtual bool checkMouse();

	inline bool isDragged  ()   const { return !!(status&STATUS_DRAGGED);   }
	inline bool isMouseOver()   const { return !!(status&STATUS_MOUSE_OVER);}
	inline bool isLeftClicked() const { return !!(status&STATUS_LEFT_CLICKED);}
	inline bool isRightClicked()const { return !!(status&STATUS_RIGHT_CLICKED);}

	//Scales the Widget and adjusts the position of the child classes 
	void scale(const float ix,const float iy);

	virtual bool checkCollision(const float ix,const float iy) const;

	inline void setActive(bool act) { active = act;}
	inline const bool isActive() const { return active;}

	inline void addEventHandler(WidgetEventHandler * e) { eventHandler = e; }
	inline void addUpdateHandler(WidgetUpdateHandler * e) { updateHandler = e; }

	inline const int getTimeStamp()         const { return timeStamp; }
	inline const int getPreviousTimeStamp() const { return previousTimeStamp;}	
	
	
	virtual inline void moveTo(const float ix,const float iy);
	virtual inline void moveBy(const float ix,const float iy);
protected:

	float x,y; //position
	float width,height; //dimentions
	const float originalWidth,originalHeight; // the original width & height before any scaling
	float lastXClick,lastYClick; //previous click mouse position
	float clickX,clickY; //current mouse click position
	unsigned int timeStamp,previousTimeStamp; // the timestamp of the current and previous mouse clicks

	Widget * parent; //a pointer this widgets parent I.E a button within a parent window
	std::map<std::string,Widget*> children;
	unsigned int flags;  //Bit mask used for the flags at start up
	unsigned int status; //bit mask for the widget status
	
	WidgetEventHandler * eventHandler; //when a widget is interacted with this calls a relevent function contained within this class
	WidgetUpdateHandler * updateHandler;

	std::string name;

	
	// deletes all the widgets in the deletion queue, returns true if one of the widgets was active at the time
	virtual bool deleteQueueWidgets();
	//Deletion queue is a queue of widgets to be deleted AFTER the checkMouse loop 
	mutable std::queue<std::string> deletionQueue;



	bool active;
	

};

#endif