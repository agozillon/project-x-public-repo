#include "AudioManager.h"
#include <iostream>

AudioManager* AudioManager::instance = NULL;

// basically just initializes an output device for all audio to be played on
AudioManager::AudioManager()
{
	/* Initialize default output device */
	if (!BASS_Init(-1, 44100, 0, 0, NULL))
	{
		std::cout << "Can't initialize device or device already Initialized" << std::endl;
	}
}

// Singleton related functions to create, delete and get the manager.
void AudioManager::createInstance()
{
	if( instance ) return;
	instance = new AudioManager();
}

void AudioManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = NULL;
}

AudioManager * AudioManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}

// loads a track in via the Audio class and adds it into the managers map for later retrieval
// takes in a bool to specifiy if it should be a looped track or not
void AudioManager::load(std::string audioName, std::string fileName, bool audioFileLooped)
{
	audioFiles.insert(std::pair<std::string, Audio*>(audioName, new Audio(fileName.c_str(), audioFileLooped)));
}

// take in a key name find it in the map and then play it
void AudioManager::playAudio(std::string audioName)
{
	audioFiles[audioName]->play();
}

// take in a key name find it in the map and then resume playing it
// from its paused position
void AudioManager::resumeAudio(std::string audioName)
{
	audioFiles[audioName]->resume();
}

// take in a key name find it in the map and then pause the audio 
void AudioManager::pauseAudio(std::string audioName)
{
	audioFiles[audioName]->pause();
}

// take in a key name and update the audio to the passed in audio value
void AudioManager::updateVolume(std::string audioName, int volume)
{
	audioFiles[audioName]->updateVolume(volume);
}

// destroy all the Audio files in the AudioManagers map
AudioManager::~AudioManager()
{
	for (std::map<std::string, Audio*>::iterator it= audioFiles.begin(); it!=audioFiles.end(); ++it)
		delete it->second;

	audioFiles.clear();
}