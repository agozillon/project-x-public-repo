#ifndef TACTICAL_MAP_MANAGER_H
#define TACTICAL_MAP_MANAGER_H
#include "TacticalMode.h"
#include "TacticalCombat.h"
#include "TacticalDeployment.h"

enum TacticalModeIdentifier
{
	DEPLOYMENT_MODE,
	COMBAT_MODE
};

// TacticalMapManager class essentially controls the internal states of the 
// Tactical Map Mode which are the TacticalCombat state and TacticalDeployment state, so basically
// a state switching class. 
class TacticalMapManager
{
public:

	static void createInstance();
	static TacticalMapManager * getInstance();
	static void deleteInstance();

	void draw();
	void update();
	void checkKeyPress();
	void changeTacticalMode(TacticalModeIdentifier id);
	TacticalCombat * getCombatMode(){return combatMode;}
	TacticalDeployment * getDeploymentMode(){return deploymentMode;}

private:

	static TacticalMapManager * instance;
	TacticalMapManager();
	~TacticalMapManager();
	TacticalMode * currentTacticalMode; 
	TacticalCombat * combatMode;
	TacticalDeployment * deploymentMode;
};

#endif