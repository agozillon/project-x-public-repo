#ifndef AUDIO_MANAGER_H
#define AUDIO_MANAGER_H
#include <string>
#include <map>
#include "Audio.h"

// AudioManager class is similar to every other Manager in this project its a 
// singleton class accessible from anywhere, it loads in the audio files, stores
// them and holds them all. It allows you to play, pause, resume and update the volume
// of all audio held by the manager by accessing the audio via a key name
class AudioManager
{
public:
	static void createInstance();
	static AudioManager * getInstance();
	static void deleteInstance();

	void load(std::string audioName, std::string fileName, bool audioFileLooped);
	void playAudio(std::string audioName);
	void resumeAudio(std::string audioName);
	void pauseAudio(std::string audioName);
	void updateVolume(std::string audioName, int volume);

private:
	static AudioManager * instance;
	AudioManager();
	~AudioManager();

	std::map<std::string, Audio*> audioFiles;
};

#endif