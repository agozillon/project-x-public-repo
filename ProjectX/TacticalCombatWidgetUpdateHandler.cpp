#include "TacticalCombatWidgetUpdateHandler.h"
#include "Text.h"
#include <sstream>

// Takes in the players units and stores there current health in an array and pointers to each of the 
// units so that we can later check how there healths been effected since the last update pass 
// and then change the on screen text if required.
TacticalCombatWidgetUpdateHandler::TacticalCombatWidgetUpdateHandler(std::vector<Unit*> pUnits){
	playerUnits = pUnits; 

	for(size_t i = 0; i < playerUnits.size(); i++)
		previousHealth.push_back(playerUnits[i]->getHealth());
}

// called via the WidgetManagers update function once attached to a WidgetManager
void TacticalCombatWidgetUpdateHandler::update(Widget *widget)
{
	Text * tmp; // creating a temporary text pointer so we can cast a Widget to a Text class
	std::ostringstream s; // string stream for streaming numbers and text into a string

	// compare the widget strings first 10 characters to the HealthText string
	// if it's the same then continue, we've essentially named all the 
	// text objects that display a units health to HealthText on creation
	// within the TacticalCombat class
	if(strncmp(widget->getName().c_str(), "HealthText", 10) == 0){

		// get the integer at the end of the widgets string, this references 
		// the position of the unit it links to in the vector
		std::string tmpString;
		for(size_t i = 10; i < widget->getName().size(); i++)
			tmpString += widget->getName()[i];

		// cast the widget to a text object as we now know its of Text type
		tmp = (Text*)widget;
		int i = atoi(tmpString.c_str()); // convert the above string integer we got into an integer
		
		// check if the health of the unit has changed at all from the last change,
		//basically a check to see if we need to do the rest. Recreating a texture every x seconds
		// for 6 things is incredibly CPU heavy.
		if(previousHealth[i] != playerUnits[i]->getHealth()){

			// if the units health is lower than 0 then its dead change the
			// string to display this, else display its current health
			if(playerUnits[i]->getHealth() <= 0)			
				s << "Dead ";  // stream data	
			else
				s << "Health " << playerUnits[i]->getHealth();  // stream data
			
			// clearing the tmpString and then assigning the stream values to it and 
			// updating the widgets on screen text for displaying the units health!
			tmpString = "";
			tmpString = s.str();
			tmp->updateText(tmpString);
			previousHealth[i] = playerUnits[i]->getHealth();
		}
	}
}

