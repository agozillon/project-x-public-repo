#include "LevelManager.h"
#include "ForestLevel.h"
#include <iostream>

LevelManager::~LevelManager()
{
	delete levelList["forest"];
}

// construct the levels on initilization and added them to the map, and assign the current
// level to the level manager
LevelManager::LevelManager()
{
	levelList.insert(std::pair<std::string, Level*>("forest", new ForestLevel()));
	currentLevel = levelList["forest"];

}

// initializes the current level via its init method
void LevelManager::init()
{
	currentLevel->init();
}

// renders the current level 
void LevelManager::draw(glm::mat4 viewProjection)
{
	currentLevel->draw(viewProjection);
}

// switches the current level to a new level if it exists via a string key
void LevelManager::switchLevel(std::string levelName)
{
	std::map<std::string, Level*>::const_iterator itr = levelList.find(levelName);

	if( itr == levelList.end() ) 
	{
		std::cout << "level does not exist" << std::endl;
		return;
	}

	currentLevel = levelList[levelName.c_str()];
}

// calls the update method of the current level, mostly for moving
// objects like particles.
void LevelManager::update()
{
	currentLevel->update();
}


