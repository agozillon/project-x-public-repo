#include "Faction.h"
#include "StrategicMapManager.h"

Faction::Faction()
{
	m_Initalized = false;
}



void Faction::Init()
{

	g_pTurnManager->AddObject(this);
	m_Initalized = true;
	m_Resources.resize(NUM_RESOURCES);

	for( unsigned int iResource = 0; iResource < m_Resources.size(); iResource++ )
	{
		m_Resources[iResource] = 0;
	}
	PrintResources();
}

Faction::Faction(const std::string&n,unsigned int i,const glm::vec4 & v)
{
	name   = n;
	index  = i;
	colour = v;
	m_Initalized = false;

}


const glm::vec4& Faction::getColour() const
{
	return colour;
}

const unsigned int Faction::getIndex() const
{
	return index;
}

const std::string & Faction::getName() const
{
	return name;
}


void Faction::RenderObjects() const
{
	for(auto itr = m_Armies.begin(); itr != m_Armies.end(); itr++)
	{
		itr->second.Render();
	}
}


void Faction::AddArmy(Army & army) 
{ 
	m_Armies[army.GetName()] = army;
	g_pTurnManager->AddObject(&m_Armies[army.GetName()]); 
}

void Faction::AddResourceTile(unsigned int ix,unsigned int iy)
{

	std::pair<float,float> data = std::pair<unsigned int,unsigned int>(ix,iy);
	auto itr = std::find(m_ResourceTiles.begin(),m_ResourceTiles.end(),std::pair<unsigned int,unsigned int>(ix,iy));

	if( itr == m_ResourceTiles.end() )
		m_ResourceTiles.push_back(data);
}

void Faction::RemoveResourceTile(unsigned int ix,unsigned int iy)
{
	auto itr = std::find(m_ResourceTiles.begin(),m_ResourceTiles.end(),std::pair<unsigned int,unsigned int>(ix,iy));
	if( itr != m_ResourceTiles.end() )
		m_ResourceTiles.erase(itr);
}


void Faction::OnTurnEnd()
{
	for( auto itr  =m_ResourceTiles.begin(); itr != m_ResourceTiles.end(); ++itr )
	{
		
		Map *map =  StrategicMapManager::getInstance()->GetMap();

		unsigned int type = map->getTile(itr->first,itr->second).getResourceType()  - 1;
		unsigned int amount = map->getTile(itr->first,itr->second).getResourceAmount();

		m_Resources[type] += amount;
	}

	PrintResources();
}


void Faction::PrintResources()
{
	printf("%s has\n",name.data());
	printf("\t%i Timber\n",m_Resources[0]);
	printf("\t%i Stone\n", m_Resources[1]);
	printf("\t%i Iron\n",  m_Resources[2]);
	printf("\t%i Coal\n",  m_Resources[3]);
}

