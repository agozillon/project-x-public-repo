#ifndef MODEL_LOADER_H
#define MODEL_LOADER_H
#include <string>
#include "Mesh.h"


enum ModelFormat
{
	UNKNOWN,
	OBJ,
	ASS
};


class ModelLoader
{
public:
	virtual const Renderable *load(const std::string &) const = 0;
	static ModelLoader * getLoader(ModelFormat);

};
#endif