#ifndef BOUNDINGBOX_H
#define BOUNDINGBOX_H
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <vector>
#include "Mesh.h"
using namespace glm;			// using glm name space so I don't have to include variablename


// basic bounding box class, takes in dimensions/position and has functions to check for collisions
// also can be used to draw the bounding box to visually represent it
class BoundingBox
{

public:
	BoundingBox(const glm::vec3 initialPosition, const glm::vec3 dimensions);		// takes in the position and dimensions of the bounding box intial position because it can change and just dimensions because they will not change
	BoundingBox(const vec3 pos, const float * meshVertices, const int numberOfVertices, const vec3 scale);	// constructor that takes in the position and dimensions of the bounding box (dimensions paramater named bDimensions as I couldn't name it dimensions since dimensions is a variable within boundingbox) 
	~BoundingBox(){}												           // blank inline destructor
	const inline vec3 getDimensions(){return dimensions;}						           // function that returns the bounding boxes current dimensions as a vec3
	const inline vec3 getPosition(){return position;}							           // inline function that returns the bounding boxes position as a vec3
	const inline vec3 getColliderPosition(){return colliderPosition;}			           // inline function that returns the colliders position before collision as a vec3, in this case the player class feeds its position in and this returns its last one not the best way but it works 
	inline void updateColliderPosition(const vec3 pos){colliderPosition = pos;}             // inline function that sets the collider(players) position before collision 
	inline void updatePosition(const vec3 pos){position = pos;}							   // inline function that updates the bounding boxes position using vec3s
	inline void updateDimensions(const vec3 dim){dimensions = dim;}				// inline function that updates the dimensions of the BoundingBox
	const bool detectBoxCollision(BoundingBox * colideeBoundingBox);		   // function that takes in a BoundingBox pointer and checks if there is a collision between the current bbox and the passed in bbox
	const bool detectRayCollision(const glm::vec3 rayPoint, const glm::vec3 rayDirection, glm::vec3 &collisionPoint); // function that checks for a ray collion
	inline void updateScale(const vec3 scalar){scale = scalar; dimensions = dimensions * scale;} // inline function that updates the bounding boxes scale 
	const vec3 seperationDistance(BoundingBox * collider);  
	// temporary test function
	void draw(mat4 viewProjection);
	
private:
	// vec3s for holding the basic variables this class requires
	vec3 dimensions;	
	vec3 position;
	vec3 colliderPosition;
	vec3 scale;
	
	// Variables entirely raelated to drawing the bounding box can remove at a later date if unrequired.
	std::string name;
	static int bCount;
	Mesh * visualBbox;
};

#endif
