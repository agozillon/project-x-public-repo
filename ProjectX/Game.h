#ifndef GAME_H
#define GAME_H
#include <SDL.h>
#include "GameManager.h"



//This contains the main game loop and sets up the inital SDL and OpenGL settings
class Game{
public:
	void run();
	Game(const int unsigned width,const int unsigned height);
	~Game();

	bool isRunning() { return running; }

	const int getHeight();
	const int getWidth();

private:
	void parseMouse(SDL_Event&);
	void init(const unsigned int width,const unsigned int height);
	void draw();
	void update();
	bool running;


	SDL_GLContext context; // OpenGL rendering context
	SDL_Window * window;  // the SDL window
	GameManager * game;
};

#endif