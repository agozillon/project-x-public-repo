#ifndef MAP_MANAGER_H
#define MAP_MANAGER_H
#include "Tile.h"
#include "RenderManager.h"

//The type of the map mode
enum MapMode
{
	TERRAIN_MAP,
	POLITICAL_MAP,
	RESOURCE_MAP
};


class MapManager
{
public:
	static void createInstance();
	static void deleteInstance();
	static MapManager* getInstance();


	//Will draw the tile as it should be drawn as given by current map mode
	void draw(const Tile*,glm::mat4& );


	//Switch the current map mode
	void switchMode(const MapMode);
	
	int GetZoom() const { return m_Zoom; }
	int GetX()    const { return m_X;    }
	int GetY()    const { return m_Y;    }


	//Moves the camera by 'amount' along the x axis
	void MoveX(int amount);  
	
	//Moves the camera by 'amount' along the y axis
	void MoveY(int amount);

	//Zooms the camera out
	void MoveZoom(int amount);
private:
	
	void checkCollision();


	//Camera parameters
	int m_X,m_Y,m_Zoom;

	int active;
	MapManager();
	~MapManager();
	static MapManager * instance;
	MapMode mode;

};

#endif