#ifndef WINDOW_WIDGET_H
#define WINDOW_WIDGET_H

#include "Widget.h"
#include "Sprite.h"
#include "WidgetEventHandler.h"


//Window specific flags, normal Widget flags still required
enum WindowWidgetFlags 
{
	WINDOW_WIDGET_NULL	    = 0x0,
	WINDOW_WIDGET_CLOSABLE  = 0x1,
	WINDOW_WIDGET_DOCKABLE  = 0x2, //inherited classes can specifiy where they are minimized to
};


//Handles the events thrown by the default buttons in the window, I.E the quit button
class WindowEventHandler: public WidgetEventHandler 
{
public:
	WindowEventHandler() { }
	virtual void handleMouseLeftClick(Widget *);
	virtual void handleDocked(Widget *);
	virtual void handleUndock(Widget *);
private:
	unsigned int previousStatus; //previous status before being docked (as WIDGET_DOCKABLE is disabled and is re-enablbed upon undock)
	float previousX,previousY;   //Previous x and y position before docked
};



//Basic window system
//Creates default buttons if flags are set
//	WINDOW_WIDGET_CLOSABLE -> creates a default close button
//  WINDOW_WIDGET_DOCKABLE -> creates both a dock and undock button
class WindowWidget: public Widget
{
public:
	WindowWidget(const std::string &name,const Sprite *, float ix,const float iy,unsigned int flags,const unsigned int );

	virtual void draw() const; 
	virtual void update();

protected:
	std::string backdropSprite;
	unsigned int windowFlags; //bitmask for the window specific flags
	float dockPositionX,dockPositionY;
};





#endif