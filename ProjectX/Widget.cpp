#include "Widget.h"
#include "GameManager.h"
#include <iostream>
#include "Mouse.h"


Widget::Widget(const std::string & n, float ix,const float iy,const float w,const float h,unsigned int f,Widget *p):
	x(ix),y(iy),width(w),height(h),flags(f),name(n),active(false),parent(p),originalHeight(h),originalWidth(w)
{
	eventHandler = nullptr;
	updateHandler = nullptr;
	status = STATUS_NULL;
}

Widget::~Widget()
{
	if( flags & WIDGET_PERFORM_CHILD_CLEANUP )
	{
		for(std::map<std::string,Widget*>::iterator itr = children.begin(); itr != children.end(); itr++)
		{
			delete itr->second;
		}

		children.clear();
	}

}

bool Widget::checkCollision(const float ix,const float iy) const 
{
	if( ix < x  ) return false;
	if( ix > x + width) return false;
	if( iy < y  ) return false;
	if( iy > y + height) return false;

	return true;
}

void Widget::removeWidget(const std::string & childName)
{

	std::map<std::string,Widget*>::const_iterator itr =  children.find(childName);
	if( itr == children.end() )
	{
		std::cout << "Error: no widget name " << childName << " already contained with in parent " << name << std::endl;
		return;
	}
	
	if( flags & WIDGET_PERFORM_CHILD_CLEANUP )
		delete itr->second;
	children.erase(itr);
}


void Widget::queueRemoveWidget(const std::string & childName) const
{
	std::map<std::string,Widget*>::const_iterator itr =  children.find(childName);
	if( itr == children.end() )
	{
		std::cout << "Error: no widget name " << childName << " already contained with in parent " << name << std::endl;
		return;
	}
	
	deletionQueue.push( itr->first);
}


void Widget::addWidget(Widget * child)
{
	if( child == nullptr )
	{
		std::cout << "Error: null child widget passed to parent widget " << name << std::endl;
		return;
	}

	if( children.find(child->getName()) != children.end() )
	{
		std::cout << "Error: child widget " << child->getName() << " already contained with in parent " << name << std::endl;
		return;
	}

	children[child->getName()] = child;
	child->parent = this;
	if( eventHandler == nullptr ) child->addEventHandler(eventHandler);
}


bool Widget::deleteQueueWidgets()
{

	bool isActiveWidget = false;
	while( !deletionQueue.empty() )
	{

		std::string childName = deletionQueue.front();
		deletionQueue.pop();

		
		std::map<std::string,Widget*>::iterator itr =  children.find(childName);

		if( itr->second->isActive() ) isActiveWidget = true;
		if( flags & WIDGET_PERFORM_CHILD_CLEANUP )
			delete itr->second;
		children.erase(itr);
	}

	return isActiveWidget;

}

bool Widget::checkMouse()
{
	//convert the integer mouse position (0->width,0->height) into the screen space coordinate frame (-1.0->1.0,-1.0->1.0)
	float screenSpaceX = (((float)Mouse::getInstance()->getX()/ ((float)GameManager::getInstance()->getWidth()*0.5f)) -1.0f);  // (x/(width/2)) - 1
	float screenSpaceY = (((float) Mouse::getInstance()->getY()/ ((float) (GameManager::getInstance()->getHeight()*0.5))) -1.0f);// (y/(height/2)) -1

	if( !checkCollision(screenSpaceX,screenSpaceY) )
	{

		status &= ~(MOUSE_ONLY_STATUS_FLAG); //Clear the mouse status flags
		return false;
	}
	
	if( eventHandler != nullptr) eventHandler->handleMouseOver(this); 

	status |= STATUS_MOUSE_OVER; //at this point the mouse is AT LEAST hovering above the Widget


	for(std::map<std::string,Widget*>::const_iterator itr = children.cbegin(); itr != children.cend(); itr++)
	{
		bool interaction = itr->second->checkMouse();
		if( interaction == true ) return true;
	}

	if( Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN )
	{
		if( eventHandler != nullptr) eventHandler->handleMouseLeftClick(this);
		
		if( !(status & STATUS_LEFT_CLICKED) )  // if not previously clicked
		{	
			
			timeStamp = Mouse::getInstance()->getTimeStamp(); // save the time stamp

			// record the position of the click
			clickX = screenSpaceX; 
			clickY = screenSpaceY;
			status |= STATUS_LEFT_CLICKED; //and set the status flag to left click being true
		}
			else  
		{
			previousTimeStamp = timeStamp;
			timeStamp = Mouse::getInstance()->getTimeStamp();

			lastXClick = clickX;
			lastYClick = clickY;	
			clickX = screenSpaceX;
			clickY = screenSpaceY;
		
			if( flags & WIDGET_DRAGABLE  )
			{
				status |= STATUS_DRAGGED; 
				moveTo(x + (clickX - lastXClick) ,y + (clickY - lastYClick));
				if( eventHandler != nullptr) eventHandler->handleDragged(this); 

			}
			
		}
	} else 
	{
		if( status & STATUS_LEFT_CLICKED )// if previously clicked
		{		
			status ^= STATUS_LEFT_CLICKED;
			status |= STATUS_MOUSE_LEFT_RELEASE;
		} else 
			status &= (~STATUS_MOUSE_LEFT_RELEASE);
		
		
	}



	if( Mouse::getInstance()->getRightState() == BUTTON_STATE_DOWN )
	{
		status |= STATUS_RIGHT_CLICKED; //and set the status flag to left click being true
	}
		else
	{
		if( status & STATUS_RIGHT_CLICKED )// if previously clicked
		{
			status |= STATUS_MOUSE_RIGHT_RELEASE;
			status ^= STATUS_RIGHT_CLICKED;
		}
		else 
			status &= (~STATUS_MOUSE_RIGHT_RELEASE);

			
	}

	deleteQueueWidgets();


	return true;

}

void Widget::scale(const float ix,const float iy)
{
	width *= ix;
	height*= iy;
	for(std::map<std::string,Widget*>::iterator itr = children.begin(); itr != children.end(); itr++)
	{
		itr->second->moveTo( x + itr->second->getX()*(ix),y + itr->second->getY()*(iy) );
	}
	
}

inline void Widget::moveTo(const float ix,const float iy)
{
	this->moveBy(ix-x,iy-y);
}

inline void Widget::moveBy(const float ix,const float iy)
{
	x += ix;
	y += iy;

	for(std::map<std::string,Widget*>::iterator itr = children.begin(); itr != children.end(); itr++)
	{
		itr->second->moveBy(ix,iy);
	}

}


Widget * Widget::getWidget(const std::string & name) const
{
	if( children.find(name) == children.end() ) return nullptr;
	return children.find(name)->second;
}



void Widget::addWidgetRelative(Widget*child)
{

}