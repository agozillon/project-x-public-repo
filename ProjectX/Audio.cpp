#include "Audio.h"
#include <iostream>

// Audio constructor basicly loads in the music file and passes in a 
// Bass flag to loop it if we want to loop the file, if not its just 
// a blank flag we pass in, it also finds a free audio channel to play the
// track on
Audio::Audio(const char* fileName, bool looped) 
{
	int flag = 0;
	if(looped == true)
		flag = BASS_SAMPLE_LOOP;
	
	// loading in the audio and checking if it worked correctly if it didn't then call c++ exit function
	if (sound=BASS_SampleLoad(FALSE, fileName, 0, 0, 3, flag))
	{
		std::cout << "sample " << fileName << " loaded!" << std::endl;
	}
	else
	{
		std::cout << "Can't load sample";
	}

	channel = BASS_SampleGetChannel(sound, FALSE);
}
	
// calls the Bass audios play function on the channel thats specific to this 
// audio file that then plays the file
void Audio::play()
{
	// if the channel can't play then output can't play sample
	if(!BASS_ChannelPlay(channel, TRUE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}
	
// same as above essentially
void Audio::resume()
{
	// if the channel can't play then output can't play sample
	if(!BASS_ChannelPlay(channel, TRUE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}

// pauses the bass audio channel, pausing the file
void Audio::pause() 
{
	BASS_ChannelPause(channel);
}

// update the audio files volume
void Audio::updateVolume(int volume)
{
	BASS_SetVolume(volume);
}