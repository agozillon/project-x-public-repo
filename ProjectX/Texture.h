#ifndef TEXTURE_H
#define TEXTURE_H
#include <GL\glew.h>

class Texture
{
public:
	Texture() {} 
	Texture(GLuint handle,const int w,const int h): width(w),height(h),handle(handle) {}

	const GLuint getHandle() const { return handle;}
	const void updateHandle(GLuint h){handle = h;}
	const int getHeight() const { return height;}
	const int getWidth() const { return width; }
	const float getHeightScreenspace() const;
	const float getWidthScreenspace() const;
private:
	GLuint handle;
	int width,height;
};

#endif