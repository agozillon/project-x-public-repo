#ifndef	TEXTTOTEXTURE_H
#define TEXTTOTEXTURE_H
#include <SDL_ttf.h> // including SDL true type font library to use ttf functions and types
#include <GL\glew.h> // including glew to use GLuints
#include <string>

// Text to Texture conversion class thats a modified version of B00234203 RT3D, setup to interface
// with the TextureManager and render to a texture stored there
class TextToTexture 
{

public:
	TextToTexture(std::string fontName, int fontSize, std::string textureToRenderName);	      // constructor that takes in a true type font and sets it up
	~TextToTexture();											  // destructor used to close the ttf_font in this case										
	void createTextTexture(const char * str, SDL_Color colour);   // function that creates a ttf_font texture from a passed in set of chars and 3 ints representing RGB and then returns it as a textureID
	
private:
	// text font pointer to hold the passed in font and string name
	// referencing the texture it renders to when createTextTexture is called
	TTF_Font* textFont;  
	std::string texture;
};

#endif
