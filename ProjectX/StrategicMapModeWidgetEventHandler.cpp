#include "StrategicModeWidgetEventHandler.h"
#include "StrategicMapManager.h"
#include "MapGenerator.h"

void StrategicModeWidgetEventHandler::handleMouseLeftClick(Widget *widget)
{
	if( widget->getName().compare("Mediterranean") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
		StrategicMapManager::getInstance()->generateNewMap(MEDITERRANEAN);
	
	if( widget->getName().compare("SmallIslands") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
		StrategicMapManager::getInstance()->generateNewMap(SMALL_ISLANDS);

	if( widget->getName().compare("LargeIslands") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
		StrategicMapManager::getInstance()->generateNewMap(LARGE_ISLANDS);

	if( widget->getName().compare("Landmass") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
		StrategicMapManager::getInstance()->generateNewMap(LANDMASS);
	
	if( widget->getName().compare("Continents") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
		StrategicMapManager::getInstance()->generateNewMap(CONTINENTS);
}