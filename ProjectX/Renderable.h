#ifndef RENDERABLE_H
#define RENDERABLE_H
#include <glm\glm.hpp>
#include <GL\glew.h>
#include <string>


/*
	A list of renderables are stored in a map within the RenderManager class, which handles when each one is called.
*/
class Renderable
{
public:
	virtual void render() const = 0;
	virtual ~Renderable() {}

	const std::string & getName() const { return name; }
	void setName(const std::string & n) const { name =n;}

protected:
	mutable std::string name; //I know, I know....

};

// so it doesn't have to be included all over the shop
#include "RenderManager.h"
#include "TextureManager.h"

#endif