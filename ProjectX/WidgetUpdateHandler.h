#ifndef WIDGET_UPDATE_HANDLER_H
#define WIDGET_UPDATE_HANDLER_H

class Widget;

// Class that enables the ability to handle update tasks such as animating
// on screen sprites or streaming text 
class WidgetUpdateHandler
{
public:
	// just a simple update template function for widgets
	virtual void update(Widget *) { }


protected:
	WidgetUpdateHandler() { } 
};


#endif