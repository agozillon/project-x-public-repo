#include "Camera.h"
#include "GameManager.h"
#define DEG_TO_RADIAN 0.017453293


// instantiates the Camera with some default rotation parameters and create the camera by using the perspective function 
// of glm via passed in variables
Camera::Camera(const glm::vec3 &e,const glm::vec3 &t, const glm::vec3& u, const float fov, const float n, const float f)
	: eye(e), target(t), up(u), yaw(0), pitch(0), FOV(fov), near(n), far(f)
{
	projection = glm::perspective(fov,(float)GameManager::getInstance()->getWidth()/(float)GameManager::getInstance()->getHeight(), n, f);
}

Camera::~Camera()
{
}

// returns the cameras look at using the glm look at functionallity its essentially
// the view matrix used in rendering
glm::mat4 Camera::getLookAt()
{
	return glm::lookAt(eye,target,up);
}

// move the cameras eye its main position
void Camera::setEye(const glm::vec3 &e)
{
	eye = e;
}

// change the cameras target, where its currently looking
void Camera::setTarget(const glm::vec3 &t)
{
	target = t;
}

// change the cameras up 
void Camera::setUp(const glm::vec3 &u)
{
	up = u;
}

// simple camera move right(or left) function from RT3D
void Camera::moveCameraRight(const float dist)
{
	eye = glm::vec3(eye.x + dist*std::cos(yaw*DEG_TO_RADIAN), eye.y, eye.z + dist*std::sin(yaw*DEG_TO_RADIAN)); // left
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}


// Camera move up function it works very basically compared to the rest(doesn't change with angle)
// which we don't want since pitch allows us to position the camera easily.
void Camera::moveCameraUp(const float dist)
{
	eye = glm::vec3(eye.x, eye.y+ dist, eye.z);
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}

// added as a zoom feature basic camera move forward from RT3D
void Camera::moveCameraForward(const float dist)
{
	eye = glm::vec3(eye.x + dist*std::sin(yaw*DEG_TO_RADIAN),  eye.y + dist * std::tan(pitch*DEG_TO_RADIAN), eye.z - dist*std::cos(yaw*DEG_TO_RADIAN));
	target = glm::vec3(eye.x + 1.0f*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0f*std::cos(yaw*DEG_TO_RADIAN));
}

// rotates the camera not actually in use as we don't require the camera
// to rotate in this project!
void Camera::rotateCamera(const float y, const float p)
{
	yaw += y;
	pitch += p;

	target = glm::vec3(eye.x + 1.0*std::sin(yaw*DEG_TO_RADIAN), eye.y + 1.0 * std::tan(pitch*DEG_TO_RADIAN), eye.z - 1.0*std::cos(yaw*DEG_TO_RADIAN));
}