#ifndef TACTICAL_DEPLOYMENT_H
#define TACTICAL_DEPLOYMENT_H
#include "TacticalMode.h"
#include "WidgetManager.h"
#include "BoundingBox.h"

// TacticalDeployment is the initial TacticalMode you enter
// when the TacticalMapState starts its a state that enables the player
// to position there units within the map and would be the linking state to the 
// strategic map that parses in the army units that are entering the battle(But
// is of course not implemented yet). 
class TacticalDeployment : public TacticalMode
{
public:
	~TacticalDeployment(){}
	TacticalDeployment();
	void draw();
	void update();
	void interaction();
	void initialDeployment(BoundingBox deploymentZone, std::vector<Unit*> units);

private:
	WidgetManager widgetManager;
	LevelManager * levelManager;
	std::vector<Unit*> playerUnits;
	std::vector<Unit*> enemyUnits;
	std::vector<Unit*> playerSelected;
};
#endif