#ifndef SHADER_H
#define SHADER_H
#include <string>
#include <GL\glew.h>

enum ShaderType
{
	VERTEX,
	FRAGMENT
};


class Shader
{
public:
	Shader(std::string name,ShaderType);
	~Shader();

	void load(std::string filePath);
	void compile();

	bool isLoaded()	  const { return loaded; }
	bool isCompiled() const { return compiled; }
	GLuint getHandle()const { return handle; }
private:
	std::string shaderName;

	GLuint handle;
	bool compiled;
	bool loaded;
	ShaderType type;


	Shader(); // override default, so shaders must have name

};

#endif