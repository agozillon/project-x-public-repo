#ifndef GAME_STATE_H
#define GAME_STATE_H

class GameState
{
public:
	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void onExit() = 0;
	virtual void onEntry() = 0;
};

#endif