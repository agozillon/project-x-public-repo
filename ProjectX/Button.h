#ifndef BUTTON_H
#define BUTTON_H
#include "Widget.h"
#include "Sprite.h" // doesn't actually use this to draw, just needs some data from it during construction


//Used to determine how to light up the mouse upon interaction (or in some case how not to)
enum ButtonIlluminationMode
{
	BUTTON_INVISIBLE = 0x0,
	BUTTON_NO_ILLUMINATION = 0x1, //button doesn't light up upon mouse over
	BUTTON_SHADER_TINT = 0x2,     //button uses the shader to lighten and darken the texture 
	BUTTON_SPRITE_SWAP = 0x4,     //button swaps between two given sprites

	BUTTON_ILLUMINATION_MODE_RESERVED = 0xFF,//the part of the bitpattern reserved for setting the *WAY* the object is shaded, the remainder is for *WHEN*
	BUTTON_ALL_EVENTS = 0x000, //for documentation purposes only
	BUTTON_ONCLICK    = 0x100, //The button will perform which ever of the above shading settings is set upon mouse click ignoring mouse over events
	BUTTON_ALWAYS     = 0x200  //The button will shade itself all the time regardless of mouse  
};

//Base button class for all other button types also intended to work as a vanilla button
class Button: public Widget
{
public:
	//Create an invible button
	Button(const std::string &name,const float ix,const float iy,const float width,const float height,unsigned int );
	Button(const std::string &name,const Sprite *, float ix,const float iy,unsigned int f,const unsigned int  i);
	Button(const std::string &name,const Sprite *,const Sprite * spriteDown,float ix,const float iy,unsigned int  f,const unsigned int  i);
	void draw() const;


	inline void setSprite(const std::string & newSprite) { sprite = newSprite;}
	inline void setSpriteDown(const std::string & newSprite) {spriteDown = newSprite; }

	inline void setIlluminationMode(const unsigned int  b) { illuminationMode = b; }


	virtual bool checkMouse() { return Widget::checkMouse(); }

protected:
	unsigned int illuminationMode;

	std::string sprite,spriteDown;
};

#endif