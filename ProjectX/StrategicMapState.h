#ifndef STRATEGIC_MAP_STATE_H
#define STRATEGIC_MAP_STATE_H
#include "GameState.h"

class StrategicMapState: public GameState
{
public:
	StrategicMapState();
	~StrategicMapState();

	void draw();
	void update();
	void onExit() { }
	void onEntry();
private:
	void checkInput();
};

#endif