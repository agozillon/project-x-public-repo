#ifndef TACTICAL_MODE_H
#define TACTICAL_MODE_H
#include "LevelManager.h"
#include "EnemyManager.h"
#include "PlayerManager.h"

// Abstract TacticalMode class that's essentially the start of another state Hierarchy
// each state that inherits from this will need to implement its own update, draw and 
// interaction functions before it can be instantiated. The states that inherit from this
// are/should be the tactical modes internal play states I.E deployment and combat
class TacticalMode
{
public:
	virtual ~TacticalMode(){}

	virtual void draw() = 0;
	virtual void update() = 0;
	virtual void interaction() = 0;

};
#endif