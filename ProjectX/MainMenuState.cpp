#include "MainMenuState.h"

#include "Sprite.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>
#include <SDL.h>
#include "GameManager.h"
#include "UnitData.h"
#include "Mouse.h"
#include "WindowWidget.h"
#include "Button.h"
#include "CheckBox.h"
#include "AudioManager.h"

MainMenuState::MainMenuState()
{
	WindowWidget * window = new WindowWidget("Window",(const Sprite*)RenderManager::getInstance()->getRenderable("Windowbackdrop"),0.0,0.0,WIDGET_DRAGABLE | WIDGET_PERFORM_CHILD_CLEANUP,WINDOW_WIDGET_CLOSABLE );
	
	window->scale(2,2);
	
	Button *button = new Button("StrategicModeButton",(const Sprite*)RenderManager::getInstance()->getRenderable("StrategicModeButton"),0.10f,0.10f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);	
	window->addWidget(button);

	button = new Button("TacticalModeButton",(const Sprite*)RenderManager::getInstance()->getRenderable("TacticalModeButton"),0.1f,0.35f,WIDGET_NULL,BUTTON_SHADER_TINT);
	button->scale(2,2);
	window->addWidget(button);

	widgetManager.addWidget(window);
	
	window = new WindowWidget("WindowTwo",(const Sprite*)RenderManager::getInstance()->getRenderable("Windowbackdrop"),-0.5,-0.5,WIDGET_DRAGABLE | WIDGET_PERFORM_CHILD_CLEANUP,WINDOW_WIDGET_CLOSABLE | WINDOW_WIDGET_DOCKABLE);
	window->scale(2,2);
	
	
	button = new Checkbox(Button("StrategicModeCheckButton",(const Sprite*)RenderManager::getInstance()->getRenderable("CheckboxUnchecked"),(const Sprite*)RenderManager::getInstance()->getRenderable("CheckboxChecked"),-0.45,-0.25,WIDGET_NULL,BUTTON_SPRITE_SWAP));
	button->scale(2,2);	
	window->addWidget(button);

	button = new Checkbox(Button("TacticalModeCheckButton",(const Sprite*)RenderManager::getInstance()->getRenderable("CheckboxUnchecked"),(const Sprite*)RenderManager::getInstance()->getRenderable("CheckboxChecked"),-0.45,0.0,WIDGET_NULL,BUTTON_SPRITE_SWAP));
	button->scale(2,2);	
	window->addWidget(button);

	
	widgetManager.addWidget(window);
}

MainMenuState::~MainMenuState()
{
}

void MainMenuState::onEntry()
{
	AudioManager::getInstance()->playAudio("MainMenuAudio");
}

void MainMenuState::onExit()
{
	AudioManager::getInstance()->pauseAudio("MainMenuAudio");
}

void MainMenuState::draw()
{
	widgetManager.draw();
}

void MainMenuState::update()
{
	this->checkInput();
}

void MainMenuState::checkInput()
{
	const Uint8 * keys = SDL_GetKeyboardState(NULL);

	if( keys[SDL_SCANCODE_S] ) GameManager::getInstance()->changeState(STRATEGIC_MAP);
	else if( keys[SDL_SCANCODE_T] ) GameManager::getInstance()->changeState(TACTICAL_MAP);


	widgetManager.checkMouse();
	


	if( widgetManager["Window"]->getWidget("StrategicModeButton")->isLeftClicked() )
	{
		GameManager::getInstance()->changeState(STRATEGIC_MAP);
	}

	if( widgetManager["Window"]->getWidget("TacticalModeButton")->isLeftClicked() )
	{
		GameManager::getInstance()->changeState(TACTICAL_MAP);
	}
	
}