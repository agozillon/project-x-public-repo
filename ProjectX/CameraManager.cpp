#include "CameraManager.h"

CameraManager * CameraManager::instance =NULL; 

// create an initial null camera so that we don't accidently call any functions on the camera
// and crash out
CameraManager::CameraManager()
{
	camera = "NULL";
	addCamera("NULL",glm::vec3(0.0f,0.0f,0.000000001f),glm::vec3(0.0f,0.0f,1.0f),glm::vec3(0.0f,1.0f,0.0f), 60.0f, 1.0f, 400.0f);

}

CameraManager::~CameraManager()
{
}

// Instantiates and adds a camera to the Manager with the passed in key string and positional variables
void CameraManager::addCamera(const std::string &name,const glm::vec3&eye,const glm::vec3&target,const glm::vec3&up, const float fov, const float n, const float f)
{
	if( cameras.find(name) != cameras.end() ) return;
	cameras[name] = new Camera(eye,target,up,fov,n,f);
}

// Set another camera with the passed in string as active, if it doesn't 
// exist then just return out
void CameraManager::setActiveCamera(const std::string & name)
{
	if( cameras.find(name) == cameras.end() ) return;
	camera = name;
}

// singleton related functions to help facilitate the creation of the CameraManager 
void CameraManager::createInstance()
{
	if( instance ) return;
	instance = new CameraManager();
	
}

void CameraManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = NULL;
	
}

CameraManager * CameraManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}

// get the cameras lookat matrix I.E its view matrix
glm::mat4 CameraManager::getLookAt()
{
	return cameras[camera]->getLookAt();
}

// set the target of the camera which is where it points to
void CameraManager::setTarget(const glm::vec3 & target)
{
	cameras[camera]->setTarget(target);
}

// set the cameras eye which is essentially where it is in the world
void CameraManager::setEye(const glm::vec3 & eye)
{
	cameras[camera]->setEye(eye);
}

// sets the cameras up direction which is as it says the up direction on the camera
// need to be careful with eye, up and at setting them pointing in the same
// direction will cause the camera to act vastly inappropriately 
void CameraManager::setUp(const glm::vec3 & up)
{
	cameras[camera]->setUp(up);
}

// call all the move camera functions, which as it says just moves it
// in the specified directions
void CameraManager::moveCameraRight( float dist)
{
	cameras[camera]->moveCameraRight(dist);
}

void CameraManager::moveCameraUp( float dist)
{
	cameras[camera]->moveCameraUp(dist);
}

void CameraManager::moveCameraForward( float dist)
{
	cameras[camera]->moveCameraForward(dist);
}

void CameraManager::rotateCamera(const float y, const float p)
{
	cameras[camera]->rotateCamera(y, p);
}