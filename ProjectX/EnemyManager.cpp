#include "EnemyManager.h"

EnemyManager::EnemyManager(std::vector<Unit*> enemyUnits)
{
	pathSystem = new PathFinder();
	// set the step distance of the PathFinding difference, higher values increase
	// the speed of the PathFinding system but increase the chances of the 
	// Pathfinder stepping through an collideable object or being unable
	// to find it's target (i.e stepping over it) so mediating this value
	// is a good idea
	pathSystem->updateStepDistance(0.8f); 

	units = enemyUnits;
	previousTime = clock();
	timePassed = 0;
}

// delete the PathFinding class and all of the units. 
EnemyManager::~EnemyManager()
{
	delete pathSystem;

	for(size_t i = 0; i < units.size(); i++)
		delete units[i];
}

// simple function that assigns targets to the units based on veeeerry simple decisions
// essentially or AI decision making code. 
void EnemyManager::targetSelection(std::vector<Unit*> playerUnits, Unit * currentUnit)
{
	std::vector<Unit*> mainTargetList;
	std::vector<Unit*> optimalTargetList;
	std::vector<glm::vec3> directionRayList;

	// add weaker units than the current units to the main target list
	for(size_t i = 0; i < playerUnits.size(); i++)
		if(*playerUnits[i] < currentUnit && playerUnits[i]->getHealth() > 0)
			mainTargetList.push_back(playerUnits[i]);

	// if the main target list is empty still add units of equal strength to the list
	if(mainTargetList.empty())
		for(size_t i = 0; i < playerUnits.size(); i++)
			if(*playerUnits[i] == currentUnit && playerUnits[i]->getHealth() > 0)
				mainTargetList.push_back(playerUnits[i]);
	

	// if the main target list is still empty then unfortunately we have to pick stronger targets
	// than the current unit to add
	if(mainTargetList.empty())
		for(size_t i = 0; i < playerUnits.size(); i++)
			if(*playerUnits[i] > currentUnit && playerUnits[i]->getHealth() > 0)
				mainTargetList.push_back(playerUnits[i]);
	

	// calculate a direction ray from the current unit to each of the current best targets, so we can check if they're
	// optimal path is occluded, which in turn makes it a less choice target
	for(size_t i = 0; i < mainTargetList.size(); i++)
		directionRayList.push_back(glm::normalize(mainTargetList[i]->getPosition() - currentUnit->getPosition()));
	
	optimalTargetList = mainTargetList;

	for(size_t i = 0; i < playerUnits.size(); i++) // for all the players units (AIs enemys)
	{
		for(size_t i2 = 0; i2 < optimalTargetList.size(); i2++) // loops for the main target list, each of which have a corresponding direction Ray
		{
			// if the ray intersects with an enemy unit and the position of the unit in the playerUnit list isn't equal to the one
			// in the main target list then remove the target from the optimal target list, as there is an object in the way and it's 
			// no longer an optimal target. Of NOTE is that the != method in this function is the pointer relevant ==/!= rather than the 
			// Unit specific == (I.E checking if the pointers are pointing to the same object, not the same unit type)
			if(playerUnits[i]->getCollisionBox()->detectRayCollision(currentUnit->getPosition(), directionRayList[i2], glm::vec3(0,0,0))
				&& playerUnits[i] != mainTargetList[i2]) {
				optimalTargetList.erase(optimalTargetList.begin()+i2);
				i2 = 0;
			}

		}	
	}

	if(!optimalTargetList.empty())
	{
		// best case scenario pick a target unit that has less health than the current unit
		for(size_t i = 0; i < optimalTargetList.size(); i++)
		{
			if(currentUnit->getHealth() > optimalTargetList[i]->getHealth())
			{
				currentUnit->setTarget(optimalTargetList[i]);
				return;
			}
		}

		// slightly worse case scenario pick one with the same health as the current unit
		for(size_t i = 0; i < optimalTargetList.size(); i++)
		{
			if(currentUnit->getHealth() == optimalTargetList[i]->getHealth())
			{
				currentUnit->setTarget(optimalTargetList[i]);
				return;
			}
		}
	
		// worst case just pick the first unit in the list that will have more hp than the current Unit
		currentUnit->setTarget(optimalTargetList[0]);
	}
	else if(!mainTargetList.empty())
		currentUnit->setTarget(mainTargetList[0]); // pick one from the main target list since none of the current
												   // set are optimal
}

void EnemyManager::update(std::vector<Scenery*> scene, std::vector<Unit*> playerUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect)
{

	timePassed += clock() - previousTime; // calculate the time since the last loop and increment the timePassed

	// simple set of loops that sets the Units target to null if its target is dead and then
	// trys to find it a new target to attack. 
	for(size_t i = 0; i < units.size(); i++)
	{
		if(units[i]->getTarget() != NULL)
			if(units[i]->getTarget()->getHealth() <= 0)
				units[i]->setTarget(NULL);
		
		if(units[i]->getTarget() == NULL)
			targetSelection(playerUnits, units[i]);
		
	}
	
	// we only really want to update these things every 3/4 a second
	if(timePassed > 750)
	{
		// combat between units and check if the players unit is dead if it is set the target to null
		for(size_t i = 0; i < playerUnits.size(); i++)
			for(size_t i2 = 0; i2 < units.size(); i2++)
				if(playerUnits[i]->getRangeBox()->detectBoxCollision(units[i2]->getCollisionBox()))
					units[i2]->calculateCombat(playerUnits[i]);

	
		// damage environmental effects
		for(size_t i = 0; i < units.size(); i++)	
			for(size_t i2 = 0; i2 < dmgEffect.size(); i2++)	
				if(units[i]->getCollisionBox()->detectBoxCollision(dmgEffect[i2]->getCollisionBox()))
					dmgEffect[i2]->IncurEnviromentalEffect(units[i]);

		// slow environmental effects
		for(size_t i = 0; i < units.size(); i++)	
			for(size_t i2 = 0; i2 < slowEffect.size(); i2++)	
				if(units[i]->getCollisionBox()->detectBoxCollision(slowEffect[i2]->getCollisionBox()))
					slowEffect[i2]->IncurEnviromentalEffect(units[i]);
				else
					units[i]->updateSlowCoefficent(1.0f);

		// rebuilds and updates its path every half a second, this is largelly
		// for the ability to lock on and follow a unit rather than path around anything.
		// Might be better to tie it into an event rather than loop through a couple of times a second
		// but this rudimentary version works.
		for(size_t i = 0; i < units.size(); i++)
		{
			if(units[i]->getTarget() != NULL && units[i]->getHealth() >= 0)
			{
				pathSystem->setTarget(units[i], units[i]->getTarget()->getPosition());
				pathSystem->path(scene);
				std::vector<glm::vec3> tmpPath;
				pathSystem->getPath(tmpPath);
				units[i]->updatePath(tmpPath);
			}
		}

		timePassed = 0;
	}

	previousTime = clock();

	// traverse the units path if its still alive!
	for(size_t i = 0; i < units.size(); i++)
		if(units[i]->getHealth() >= 0)
			units[i]->traversePath(scene);
		
}

// render the enemy units to screen
void EnemyManager::draw(glm::mat4 viewProjection)
{
	for(size_t i = 0; i < units.size(); i++)
	{
		units[i]->draw(viewProjection);
	}
}
