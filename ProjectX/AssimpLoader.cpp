#include "AssimpLoader.h"
#include <postprocess.h>
#include <scene.h>
#include <mesh.h>
#include <Importer.hpp>
#include <iostream>
#include <vector>

// basic load function had help from an online tutorial to create it
const Renderable * AssimpLoader::load(const std::string &filePath) const {
	Assimp::Importer importer;

	// reads in the file to a scene object
	const aiScene* scene = importer.ReadFile(filePath.c_str(), aiProcess_Triangulate | aiProcess_GenSmoothNormals | aiProcess_FlipUVs);
	
	std::vector<float> vertices;
	std::vector<unsigned short> indices;
	std::vector<float> normals;
	std::vector<float> texCoords;

	// if it downloaded appropriately
	if(scene)
	{
		const aiMesh * mesh = scene->mMeshes[0];
		const aiVector3D blankVal(0.0f, 0.0f, 0.0f);
		
		// get the verts, nomrals and tex coords of the mesh in the scene and push them
		// into a vector for each type
		for(unsigned int i = 0; i < mesh->mNumVertices; i++){
			const aiVector3D * vert = &(mesh->mVertices[i]);
			const aiVector3D * normal = &(mesh->mNormals[i]);
			const aiVector3D * texCoord;

			if(mesh->HasTextureCoords(0)){
				texCoord = &(mesh->mTextureCoords[0][i]);
			}else{
				texCoord = &blankVal;
			}

			vertices.push_back(vert->x);
			vertices.push_back(vert->y);
			vertices.push_back(vert->z);

			texCoords.push_back(texCoord->x);
			texCoords.push_back(texCoord->y);
			
			normals.push_back(normal->x);
			normals.push_back(normal->y);
			normals.push_back(normal->z);
		}
	
		// get the indices of the model, make sure there is 3 per face so we're missing no data
		for(unsigned int i = 0; i < mesh->mNumFaces; i++){
			const aiFace& face = mesh->mFaces[i];
				
			if(face.mNumIndices != 3)
				std::cout << "Possible Error: Incorrect number of indices for face " << std::endl;
				
			indices.push_back(face.mIndices[0]);
			indices.push_back(face.mIndices[1]);
			indices.push_back(face.mIndices[2]);
		}
	}else{
		std::cout << "model unloadable or non-existant " << std::endl;
	}

	// create a mesh with this data and then return it
	return new Mesh(&vertices[0], vertices.size(),&texCoords[0], texCoords.size(),&indices[0],indices.size(),&normals[0], normals.size());
}
