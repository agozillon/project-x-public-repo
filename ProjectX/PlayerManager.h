#ifndef PLAYER_MANAGER_H
#define PLAYER_MANAGER_H
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Pathfinder.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"

// This classes job is to manage the players units it bacially takes in all the Units that relate to the player
// and controls there updating and rendering and more player specific tasks for the units such as checking for
// mouse commands and then actings on them! The PlayerManager and EnemyManager were made to essentially break up
// the two different types of Unit control as the AI and Player control there units in two distinct manners. It
// also makes extending these classes easier if we need to add any player specific or AI specific code, for example
// extending the AI code to make its difficulty level a bit more playable. 
class PlayerManager {
public:
	PlayerManager(std::vector<Unit*> playerUnits);
	~PlayerManager();
	void draw(glm::mat4 viewProjection);
	void update(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect);
	void interaction(std::vector<Scenery*> scene, std::vector<Unit*> enemyUnits);
	void getUnits(std::vector<Unit*> &u){u = units;}
	void updatePlayerSelected(Unit* playerSelection){playerSelected = playerSelection;}

private:
	std::vector<Unit*> units; // all the player owned units

	Unit * playerSelected; // the players currently selected unit

	PathFinder * pathSystem; // the pathfinding system for the PlayerManager

	clock_t previousTime;   // variables to hold the update times so we can calculate when 0.75 ms have passed, before calling path finding again
	clock_t timePassed;
};
#endif
