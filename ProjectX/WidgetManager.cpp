#include "WidgetManager.h"
#include <iostream>

WidgetManager::WidgetManager(): activeWidget(nullptr),Widget("Manager",-1.0,-1.0,2.0,2.0,WIDGET_PERFORM_CHILD_CLEANUP)
{

}


void WidgetManager::draw() const
{
	for(std::map<std::string,Widget*>::const_iterator itr = children.cbegin(); itr != children.cend(); itr++)
	{
		itr->second->draw();
	}
}

void WidgetManager::update() const
{
	for(std::map<std::string,Widget*>::const_iterator itr = children.cbegin(); itr != children.cend(); itr++)
	{
		itr->second->update();
	}
}

bool WidgetManager::checkMouse()
{


	if( activeWidget != nullptr && activeWidget->checkMouse())
	{
		if( deleteQueueWidgets() )
			activeWidget = nullptr;
		return true; 
	}


	for(std::map<std::string,Widget*>::const_iterator itr = children.cbegin(); itr != children.cend(); ++itr)
	{
		if( itr->second->checkMouse() ) 
		{
			itr->second->setActive(true);
			activeWidget = itr->second;
			break;
		} else if( itr->second->isActive() )
		{
			itr->second->setActive(false);
			activeWidget = nullptr;
			return false;
		}
	}

	if( !activeWidget )  // No active widget
		return false;

	if( deleteQueueWidgets() )
		activeWidget = nullptr;
	return true;
}