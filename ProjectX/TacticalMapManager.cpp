#include "TacticalMapManager.h"

#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>
#include <glm\gtc\type_ptr.hpp>


#include "ShaderManager.h"
#include "CameraManager.h"
#include "RenderManager.h"
#include "TextureManager.h"
#include "GameManager.h"
#include "Scenery.h"
#include "Mouse.h"

#include <SDL.h>
#include <stdio.h>

TacticalMapManager* TacticalMapManager::instance = NULL;

void TacticalMapManager::createInstance()
{
	if( instance ) return;

	instance = new TacticalMapManager();
}

void TacticalMapManager::deleteInstance()
{

	if( !instance) return;

	delete instance;
	instance = NULL;
}

TacticalMapManager* TacticalMapManager::getInstance()
{
	if( !instance ) createInstance();

	return instance;
}

// Basic function that takes in an TacticalModeIdentifier Enum and then
// switches the currently active state
void TacticalMapManager::changeTacticalMode(TacticalModeIdentifier id)
{
	if(id == DEPLOYMENT_MODE)
		currentTacticalMode = deploymentMode;
	
	if(id == COMBAT_MODE)
		currentTacticalMode = combatMode;
}

// Constructer that sets up the Basic OpenGL commands, shader and camera in preperation
// for rendering the TacticalMapMode in 3D
TacticalMapManager::TacticalMapManager()
{
	// Add a camera to the Camera Manager that's setup in the correct position and orientation for the Tactical Map Mode and then activate it
	CameraManager::getInstance()->addCamera("TacticalMap", vec3(0.0f, 0.0f, 0.0),vec3(0.0f, 0.0f, -1.0f),vec3(0.0f, 1.0f, 0.0f), 60.0f, 1.0f, 400.0f);
	CameraManager::getInstance()->setActiveCamera("TacticalMap");
	CameraManager::getInstance()->rotateCamera(0.0f, 30.0f);

	ShaderManager::getInstance()->use("simpleTac"); //just the same as usual glUse(program)

	// initialize the TacticalMode's and then set the deploymentMode as our initial mode. 
	deploymentMode = new TacticalDeployment();
	combatMode = new TacticalCombat();
	currentTacticalMode = deploymentMode; 
	
	// activating the various OpenGL functions to render in 3D, I.E Depth testing, Blending with the Alpha Channel
	// and activating the Depth buffer.
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(GL_TRUE);
}

TacticalMapManager::~TacticalMapManager()
{
	delete deploymentMode;
	delete combatMode;
}

// render the current tactical mode state
void TacticalMapManager::draw()
{
	currentTacticalMode->draw();
}

// update the current tactical mode state
void TacticalMapManager::update()
{
	currentTacticalMode->update();
}

// check for key presses (and mouse interaction) for the current
// tactical mode
void TacticalMapManager::checkKeyPress()
{
	currentTacticalMode->interaction();	
}