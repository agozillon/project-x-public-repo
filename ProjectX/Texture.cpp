#include "Texture.h"
#include "GameManager.h"

const float Texture::getHeightScreenspace() const
{
	return ( (float)height/ ((float)GameManager::getInstance()->getHeight())); 
}

const float Texture::getWidthScreenspace() const
{
	return ( (float)width/ ((float)GameManager::getInstance()->getWidth())); 
}