#include "PathFinder.h"


// comparator function for the std::sort called in the adjacentNodes function
bool comparator(PathNode * param, PathNode * param2){
		if(param->getF() < param2->getF())
			return true;
		else
			return false;
}

void PathFinder::updateStepDistance(float stepDist)
{
	stepDistance = stepDist;
	stepStraightCost = stepDist;
	stepDiagonalCost = sqrt(stepDist * stepDist + stepDist * stepDist); // pythagoras theorum to find the diagonal step cost I.E moving 1 up and across then calculating the hypotenuse 
	bias = stepStraightCost / 2; // bias+bias = step cost so we add/negate it from the goal value making it always in reach/around   
}

// for all heruistics we ignore the Z the up value as it throws off the herustic estimation by overestimating! which we don't want to do
// however we do not want to get it exact we infact want to underestimate it otherwise we get insane slow down!

// manhattan distance herustic
float PathFinder::manhattanHerusticCost(glm::vec3 currentNode, glm::vec3 goalPosition)
{
	goalPosition.y = 0;
	currentNode.y = 0;

	return abs(glm::length(goalPosition - currentNode));
}


// euclidian distance herustic, fun experiment: try to switch this out with the manhattan herustic and see how ridiculously slow it is due to it 
//being an exact estimate (other situations it might work not here), diagonal distance also causes major herustic miscalculation
float PathFinder::euclidianHerusticCost(glm::vec3 currentNode, glm::vec3 goalPosition)
{
	float dx = abs(currentNode.x - goalPosition.x);
	float dy = abs(currentNode.y - goalPosition.y);

	return sqrt(dx * dx + dy * dy);
}

// gets and adds the adjacent nodes to the open list, the open list is an ordered vector with the first always being
// the PathNode with the smallest F. It also checks if the PathNodes are in the OpenList and ClosedList already or if they collide with an object
// if they are it removes the new adjacent and in certain cases recalculates the old version
void PathFinder::adjacentNodes(std::vector<PathNode*> &oList, std::vector<PathNode*> &cList, PathNode * parent, std::vector<Scenery*> obstacles)
{
	PathNode * newNodes[8]; 

	// the + 1 for the G is the cost of "moving" to the next node, Ideally we'd have a different value for each terrain type
	// to make the A* algorithm favour other terrain types over others. Took out the Z when factoring in the heruistic as that actually throws off the accuracy of it as we're not using the Z(up/down value) within our 3D RTS as we only move on
	// a one plane
	newNodes[0] = new PathNode(parent->getG()+stepStraightCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x, parent->getPosition().y+stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x, parent->getPosition().y+stepDistance, parent->getPosition().z), parent);
	newNodes[1] = new PathNode(parent->getG()+stepStraightCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x, parent->getPosition().y-stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x, parent->getPosition().y-stepDistance, parent->getPosition().z), parent);
	newNodes[2] = new PathNode(parent->getG()+stepStraightCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y, 0), targetPos), glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y, parent->getPosition().z), parent);
	newNodes[3] = new PathNode(parent->getG()+stepStraightCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y, 0), targetPos), glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y, parent->getPosition().z), parent);
	newNodes[4] = new PathNode(parent->getG()+stepDiagonalCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y+stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y+stepDistance, parent->getPosition().z), parent);
	newNodes[5] = new PathNode(parent->getG()+stepDiagonalCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y+stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y+stepDistance, parent->getPosition().z), parent);
	newNodes[6] = new PathNode(parent->getG()+stepDiagonalCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y-stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x+stepDistance, parent->getPosition().y-stepDistance, parent->getPosition().z), parent);
	newNodes[7] = new PathNode(parent->getG()+stepDiagonalCost, euclidianHerusticCost(glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y-stepDistance, 0), targetPos), glm::vec3(parent->getPosition().x-stepDistance, parent->getPosition().y-stepDistance, parent->getPosition().z), parent);
	
	bool nodeClosed = false;

	for(int i = 0; i < 8; i++)
	{
		// updating colider position to the path nodes position and then checking if it collides with any obstacles at this Node point
		// if it does then this node is closed
		activeUnit->getCollisionBox()->updatePosition(glm::vec3(newNodes[i]->getPosition().x, newNodes[i]->getPosition().y, activeUnit->getPosition().z));
		
		// check if the node is inside an obstacle that's collideable if it is then its not valid.
		for(size_t j = 0; j < obstacles.size(); j++)
		{
			if (activeUnit->getCollisionBox()->detectBoxCollision(obstacles[j]->getCollisionBox()) && obstacles[j]->getUnitCollideable() == true)
			{
				nodeClosed = true;

			}
		}

		// check that there is no duplicates in the closed list or open list 
		// (I.E it's not been checked yet or isn't already in line to be checked) 
		for(size_t j = 0; j < cList.size(); j++)
		{	
			if(*newNodes[i] == *cList[j])
			{
				nodeClosed = true;

				// if it's g score (move score from current node to the duplicate node) is less than
				// the old g score (from a previous node to this new node), it's currently a bit of
				// an estimate as we're always assuming that it's a straight step from the current node
				// to the old node position
				if((parent->getG() + stepStraightCost) < cList[j]->getG())
				{
					// update its G score to the newer version from the current node
					// and its parent node
					cList[j]->updateG(parent->getG() +stepStraightCost); 
					cList[j]->updateParent(parent);
					oList.push_back(cList[j]);
					cList.erase(cList.begin()+j);
				}
			}
		}
		
		
		for(size_t j = 0; j < oList.size(); j++)
		{	
			if(*newNodes[i] == *oList[j])
			{
				nodeClosed = true;

				// same as above
				if((parent->getG() + stepStraightCost) < oList[j]->getG())
				{
					// update its G score to the newer version from the current node
					// and its parent node
					oList[j]->updateG(parent->getG() + stepStraightCost); 
					oList[j]->updateParent(parent);
				}
			}
		}
		
		// if it isn't already in either list add it to the open list
		if(nodeClosed == false)
		{
			oList.push_back(newNodes[i]);
		}
		else // else clean up the node and set the bool back to false 
		{
			delete newNodes[i];
			nodeClosed = false;
		}
	}

	// sort the PathNodes by F into ascending order
	sort(openList.begin(), openList.end(), comparator);
}

void PathFinder::retracePath(std::vector<glm::vec3> &path, PathNode * endNode)
{
	PathNode * tmp = endNode;
	PathNode test;

	while(tmp->getParent() != NULL){
		path.push_back(tmp->getPosition());
		tmp = tmp->getParent();
	}
}

void PathFinder::path(std::vector<Scenery*> obstacles)
{
	bool goalReached = false;

	PathNode * initialPos = new PathNode(0, euclidianHerusticCost(activeUnit->getPosition(), targetPos), activeUnit->getPosition(), NULL);

	glm::vec3 oldPosition = activeUnit->getCollisionBox()->getPosition();
	activeUnit->getCollisionBox()->updatePosition(targetPos);
	
	// trying too quicken the vector a little to optimize it, most paths won't take anywhere near 3k
	// which means no resizing and copying, so speed at the cost of some memory 
	openList.reserve(3000); 
	closedList.reserve(3000);
	
	//currently not fully functional but useable it will make Units extremely near Scenery untouchable!
	// loop to check if the initial position is inside an object as we can't move into an object
	// return before time is wasted! Also checks if the object is collideable so we can switch pathing collision with scenery on and off
	// mostly useful for floor tiles as just about all the units collision box will at least touch the ground for realism
	for(size_t i = 0; i < obstacles.size(); i++)
	{
		if(obstacles[i]->getCollisionBox()->detectBoxCollision(activeUnit->getCollisionBox()) && obstacles[i]->getUnitCollideable() == true)
		{
			activeUnit->getCollisionBox()->updatePosition(oldPosition); // set the bbox back to its originally position and then return out
			delete initialPos;
			return;
		}
	}
	
	// if it hasn't exited position is valid so add it to the open list!
	openList.push_back(initialPos);

	int stepCount = 0; // number of Steps/loops we've went through, we wish to cancel out if it gets too high as 
	                   // the path may not be easily attainable via the currentStep distance (it's rare but happens)
	do{
		// get best/most likely node in list take it, set it as current node, add it to the closed list and remove from open
		PathNode * currentNode = openList.at(0);
		closedList.push_back(currentNode);
		openList.erase(openList.begin());

		// if current nodes pos is equal to the target pos then we've reached the goal node, we disregard the Z
		// as we should only ever be moving in two directions (as it's an RTS) and some objects may be displaced
		// slightly above 0 Z. We also give a bias for floating point errors.
		if((currentNode->getPosition().x >= targetPos.x - bias && currentNode->getPosition().x <= targetPos.x + bias) && (currentNode->getPosition().y >= targetPos.y - bias && currentNode->getPosition().y <= targetPos.y + bias)) 
		{
			// get the retraced path from the goal point
			retracePath(currentPath, currentNode);

			goalReached = true; 
		}
		else// get adjacent nodes for the open list as the current node is not the goal!
		{
			adjacentNodes(openList, closedList, currentNode, obstacles);
			stepCount++;
		}

		
	} while(!openList.empty() && goalReached == false && stepCount < maxStepCount); // cancel out when all nodes have been visited, goals been reached or stepcount is rather large
		// if the step size is 0.1 some things may take more than 8000 steps, however if it does take 8000 it's going far too slowly anyway!

	// delete all the PathNode objects in the closed and open list
	for(size_t i = 0; i < closedList.size(); i++)
		delete closedList[i];
			
	for(size_t i = 0; i < openList.size(); i++)
		delete openList[i];

	// delete all references from the vectors
	openList.clear();
	closedList.clear();

	activeUnit->getCollisionBox()->updatePosition(oldPosition);						  
}