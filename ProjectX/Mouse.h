#ifndef MOUSE_H
#define MOUSE_H
#include <SDL.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

enum MouseButtonState
{
	BUTTON_STATE_DOWN,
	BUTTON_STATE_UP
};

struct Ray
{
	glm::vec3 rayOrigin;
	glm::vec3 rayDirection;
};

// basic mouse class that uses ray casting that can be used to pick objects from a 3D world
// or even work within a 2D world
class Mouse
{

public:
	// standard Singleton function and setup
	static void createInstance();  
	static void deleteInstance();
	static Mouse * getInstance();

	// function for calculating the ray, saves results into rayOrigin and rayDirection
	Ray calculateRay();

	void updateEvent(const SDL_Event e);
	const inline SDL_Event & getEvent() const { return currentEventState; }

	const inline MouseButtonState getRightState() const { return right; }
	const inline MouseButtonState getLeftState()  const { return left;  }
	const inline MouseButtonState getMiddleState()const { return middle;}

	const inline int getTimeStamp() const { return timeStamp; }
	const inline int getX() const { return x; }
	const inline int getY() const { return y; }

private:
	// holds the currentEventState so the class can access, variables from it and check it
	// if required
	SDL_Event currentEventState;
	int x,y;
	unsigned int timeStamp;
	
	MouseButtonState right,left,middle;
	
	// private constructor/destructor as singletons do, static mouse instance to link to itself once
	// instantiated
	static Mouse * instance;
	Mouse();
	~Mouse();
};


#endif
