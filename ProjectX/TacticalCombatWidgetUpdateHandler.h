#ifndef TACTICAL_COMBAT_WIDGET_UPDATE_HANDLER_H
#define TACTICAL_COMBAT_WIDGET_UPDATE_HANDLER_H
#include "WidgetUpdateHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "LevelManager.h"


// TacticalCombatWidgetUpdateHandler is a widget handler specifically made for the 
// TacticalCombat states GUI, it's essentially for attaching to the Window and Text within
// that state. It's main job is to update the TacticalCombat GUIs health text that's displayed
// on screen for each unit. It gets continually updates as each of the units gets damaged.
class TacticalCombatWidgetUpdateHandler: public WidgetUpdateHandler
{
public: 
	TacticalCombatWidgetUpdateHandler(std::vector<Unit*> pUnits); 
	void update(Widget *);

private:
	std::vector<Unit*> playerUnits;
	std::vector<int> previousHealth;
};

#endif