#ifndef FACTION_H
#define FACTION_H
#include <string>
#include "RenderManager.h"
#include "Army.h"
#include <map>
#include <algorithm>
#include "TurnListener.h"
#include "ResourceManager.h"
/*
	Faction class, managed by FactionManager class
	the index is directly related to the index in tile
	colour is used to colour in the political map
*/
class Faction: public TurnListener
{
public:

	Faction(const std::string &,unsigned int index,const glm::vec4&);
	~Faction();



	void Init();
	void ClearTiles() { m_ResourceTiles.clear(); }
	void OnTurnEnd();


	void PrintResources();
	//Render all game objects owned by this faction
	void RenderObjects() const;
	const glm::vec4 & getColour() const;
	const unsigned int getIndex() const;
	const std::string &getName() const;


	bool IsInitalized() const { return m_Initalized; }

	void AddArmy(Army & army);
	void AddResourceTile(unsigned int ix,unsigned int iy);
	void RemoveResourceTile(unsigned int ix,unsigned int iy);


	Army & GetArmy(const std::string & name) { return m_Armies[name]; }
private:
	std::string name;
	unsigned int index;
	glm::vec4 colour;

	std::map<std::string,Army> m_Armies;

	//Tiles under the influence of this faction that also contain a resource
	std::vector<std::pair<unsigned int,unsigned int>> m_ResourceTiles;
	


	std::vector<unsigned int> m_Resources;
	Faction();

	bool m_Initalized;
};

#endif