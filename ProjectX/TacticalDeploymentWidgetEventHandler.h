#ifndef TACTICAL_DEPLOYMENT_WIDGET_EVENT_HANDLER_H
#define TACTICAL_DEPLOYMENT_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"
#include "Unit.h"
#include "LevelManager.h"

// TacticalDeploymentWidgetEventHandler is a widget handler specifically made for the 
// TacticalDeployment states GUI, it's essentially for attaching to the Window and buttons
// that are created in that state. It then gets looped through continually by the widgetManager
// and checks for any left click events. If any occur the code in handleMouseLeftClick is executed.
class TacticalDeploymentWidgetEventHandler: public WidgetEventHandler
{
public: 
	TacticalDeploymentWidgetEventHandler(LevelManager * lManager, std::vector<Unit*> * pUnits, std::vector<Unit*> * eUnits) {levelManager = lManager; playerUnits = pUnits; enemyUnits = eUnits;}
	void handleMouseLeftClick(Widget *);

private:
	// stores all of the unit and level data that gets created in the TacticalDeployment state
	// this is so we can pass it onto the TacticalCombat class via this EventHandler
	std::vector<Unit*> * playerUnits;
	std::vector<Unit*> * enemyUnits;
	LevelManager * levelManager;
};

#endif