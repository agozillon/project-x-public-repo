#include "TacticalCombat.h"

#include "CameraManager.h"
#include "GameManager.h"
#include "Mouse.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"
#include "WindowWidget.h"
#include "TacticalCombatWidgetEventHandler.h"
#include "TacticalCombatWidgetUpdateHandler.h"
#include "Button.h"
#include "Text.h"
#include <sstream>


TacticalCombat::TacticalCombat()
{

}

// setMap is basically a function that gets called apon switching to the TacticalCombat map
// via the TacticalDeployment's widget event handler it takes in all the units and the LevelManager
// and then passes the units into EnemyManager and PlayerManager and assigns the LevelManager to its
// own LevelManager pointer. It also sets up the GUI for the TacticalCombat mode
void TacticalCombat::setMap(LevelManager * lManager, std::vector<Unit*> pUnits, std::vector<Unit*> eUnits)
{
	playerManager = new PlayerManager(pUnits);
	enemyManager = new EnemyManager(eUnits);
	levelManager = lManager;

	// creating the window widget and event/update handlers for the TacticalCombat mode and then
	// attaching them to the Window
	WindowWidget * window = new WindowWidget("Window",(const Sprite*)RenderManager::getInstance()->getRenderable("Windowbackdrop"),-0.8f, -1.0, WIDGET_PERFORM_CHILD_CLEANUP, WINDOW_WIDGET_NULL );
	window->scale(5.0f,0.7f);
	TacticalCombatWidgetEventHandler * handler = new TacticalCombatWidgetEventHandler(playerManager, pUnits);
	TacticalCombatWidgetUpdateHandler * updateHandler = new TacticalCombatWidgetUpdateHandler(pUnits);
	widgetManager.addEventHandler(handler);
	widgetManager.addUpdateHandler(updateHandler);
	
	// temp pointers and string stream to help create the GUI
	Button *button;
    Text * text;	
	std::ostringstream s;
	
	// basically just creates buttons and text for each of the "Cards" that are displayed on the UI
	// that link to the Units when clicked
	for(size_t i = 0; i < pUnits.size(); i++)
	{
		// creating the background card
		s << "Card" << i;  // stream data	
		std::string tmp(s.str());
		button = new Button(tmp, (const Sprite*)RenderManager::getInstance()->getRenderable("CardBackground"),-0.7f + (i * 0.25), -0.98f,WIDGET_NULL,BUTTON_SHADER_TINT);
		button->scale(0.20, 0.25);	
		button->addEventHandler(widgetManager.getEventHandler());
		window->addWidget(button);
		
		s.str(""); // clear
		s << "Text" << i;
		tmp = s.str();
		
		// Creating the units name type
		s << "RenderTarget" << i;
		std::string renderTarget(s.str());
		s.str("");
		text = new Text(tmp, renderTarget, "MavenPro-Regular.ttf", pUnits[i]->getUnitType(), 12, 0, 0, 0, -0.5f + (i * 0.25), -0.78f, 1, 1, WIDGET_NULL);
		text->scale(-0.2,0.05);
		text->addEventHandler(widgetManager.getEventHandler());
		window->addWidget(text);

		// creating the Health display
		s.str(""); // clear
		s << "HealthText" << i;
		tmp = s.str();
	
		s << "RenderTargetHealth" << i;
		renderTarget = s.str();
		s.str("");

		s << "Health " << pUnits[i]->getHealth();
		std::string health(s.str());
		s.str("");

		text = new Text(tmp, renderTarget, "MavenPro-Regular.ttf", health, 12, 225, 225, 0, -0.5f + (i * 0.25), -0.98f, 1, 1, WIDGET_NULL);
		text->scale(-0.2,0.05);
		text->addUpdateHandler(widgetManager.getUpdateHandler());
		window->addWidget(text);
	}

	widgetManager.addWidget(window);
}

// renders the various elements needed to render in the TacticalCombat mode
void TacticalCombat::draw()
{
	glDepthMask(GL_TRUE);

	glm::mat4 projection = CameraManager::getInstance()->getProjection(); // usual GLM stuff
	glm::mat4 view = CameraManager::getInstance()->getLookAt();
	glm::mat4 viewProjection = projection * view;

	
	levelManager->draw(viewProjection);
	playerManager->draw(viewProjection);
	enemyManager->draw(viewProjection);
	widgetManager.draw();
}

// updates all the TacticalCombat objects, a lot of variable passing into managers
// required
void TacticalCombat::update()
{
	levelManager->update(); // update the level, moving objects etc.

	std::vector<Scenery*> scene;	
	std::vector<DamageEnvironmentalEffect*> tmpDmg;
	std::vector<SlowEnvironmentalEffect*> tmpSlow;

	levelManager->getDamageEnvironmentalEffects(tmpDmg);
	levelManager->getSlowEnvironmentalEffects(tmpSlow);
	levelManager->getScenery(scene);
	
	std::vector<Unit*> tmpUnits;


	enemyManager->getUnits(tmpUnits);
    playerManager->update(scene, tmpUnits, tmpDmg, tmpSlow);
	tmpUnits.clear();

	playerManager->getUnits(tmpUnits);
	enemyManager->update(scene, tmpUnits, tmpDmg, tmpSlow);
	widgetManager.update();
}

void TacticalCombat::interaction()
{
	Uint8 * keys = SDL_GetKeyboardState(NULL);

	// move the camera to the left/right/up/down if a WASD key is pressed or the mouse is
	// at the edge of the screen. Each of the mouse at edge of screen checks have a different 
	// offset because using the WASD keys to move the screen actually jumps the mouse co-ordinates
	// to invalid values which coincidently go below the threshold in some cases making the screen
	// continue to scroll until the mouse is moved again. The various offsets stop this whilst keeping
	// the scrolling sensitive.
	if( keys[SDL_SCANCODE_W] || Mouse::getInstance()->getY() >= GameManager::getInstance()->getHeight() - 10)  
	{
		CameraManager::getInstance()->moveCameraUp(0.1f);
	}

	if( keys[SDL_SCANCODE_S] || Mouse::getInstance()->getY() <= 10)
	{
		CameraManager::getInstance()->moveCameraUp(-0.1f);
	}

	if( keys[SDL_SCANCODE_A] || Mouse::getInstance()->getX() <=  4 ) 
	{
		CameraManager::getInstance()->moveCameraRight(-0.1f);
	}

	if( keys[SDL_SCANCODE_D] || Mouse::getInstance()->getX() >= GameManager::getInstance()->getWidth() - 5)
	{
		CameraManager::getInstance()->moveCameraRight(0.1f); 
	}

	if( keys[SDL_SCANCODE_Z] )
	{
		CameraManager::getInstance()->moveCameraForward(0.1f);
	}

	if( keys[SDL_SCANCODE_X])
	{
		CameraManager::getInstance()->moveCameraForward(-0.1f);
	}

	if( keys[SDL_SCANCODE_COMMA] )
	{
		CameraManager::getInstance()->rotateCamera(0.0f, -1.0);
	}

	if( keys[SDL_SCANCODE_PERIOD] )
	{
		CameraManager::getInstance()->rotateCamera(0.0f, 1.0f);
	}

	std::vector<Scenery*> scene;
	levelManager->getScenery(scene);
	std::vector<Unit*> tmpUnits;
	enemyManager->getUnits(tmpUnits);

	// basically just blocking other interactions happening if the GUIs been clicked
	if(!widgetManager.checkMouse())
		playerManager->interaction(scene, tmpUnits); // all the main player interaction with units happens in PlayerManager
	
	
	
	if( keys[SDL_SCANCODE_ESCAPE]) GameManager::getInstance()->changeState(MAIN_MENU);
}

TacticalCombat::~TacticalCombat()
{
	delete levelManager;
	delete enemyManager;
	delete playerManager;
}