#ifndef TILE_MANAGER_H
#define TILE_MANAGER_H
#include "Tile.h"

class TileManager
{
public:
	static void createInstance();
	static TileManager * getInstance();
	static void deleteInstance();
	
	void draw(const Tile *tile);


private:
	static TileManager * instance;
	TileManager();
};

#endif