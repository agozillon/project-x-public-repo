#include "TextureLoader.h"


TextureLoader * TextureLoader::getLoader(ImageType i)
{
	if( i == BMP)
		return new BMPLoader();

	std::cout << "Error: unknown imagetype" << std::endl;
	return NULL;
}