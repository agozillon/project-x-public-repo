#include "Text.h"
#include "RenderManager.h"
#include "GameManager.h"

// Constructor that takes in all the usual widget related pass ins, on top of a string as a render target, this string can not be 
// an existing texture key otherwise it will be overwritten by the TextToTexture class! Other than that it takes a TTF font name
// the text and the texts colour and then creates the Texture and renderable combination to render on screen as the widget
Text::Text(const std::string &name, const std::string textureRenderTargetName, const std::string fontName, std::string displayText, const int fontSize, Uint8 r, Uint8 g, Uint8 b, float ix,const float iy, float w, float h, unsigned int f)
	: Widget(name,ix,iy,w,h,f)
{
	text = new TextToTexture(fontName, fontSize, textureRenderTargetName);
	colour.r = r; colour.g = g; colour.b = b;
	text->createTextTexture(displayText.c_str(), colour); 
	RenderManager::getInstance()->addRenderable(textureRenderTargetName, new Sprite(textureRenderTargetName));
	sprite = textureRenderTargetName;
}

// updates the current widgets text with the new string by calling thecreateTextTexture
// function of the TextToTexture object again
void Text::updateText(std::string newText){
	text->createTextTexture(newText.c_str(), colour);
}

// basically renders the Widget like every other widget, no fancy code here!
void Text::draw() const
{
	glm::mat4 mvp(1.0f);

	mvp = glm::translate(mvp,glm::vec3(x,y,0));
	mvp = glm::scale(mvp,glm::vec3(width,height,0));


	glDepthMask(GL_FALSE);

	ShaderManager::getInstance()->use("GUI");
	ShaderManager::getInstance()->getShader("GUI")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	ShaderManager::getInstance()->getShader("GUI")->setUniform1f("tint",1.0f);
	RenderManager::getInstance()->renderRenderable(sprite);


	glDepthMask(GL_TRUE);

}

//delete the TextToTexture object
Text::~Text()
{
	delete text;
}