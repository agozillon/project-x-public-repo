#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H
#include "TextureLoader.h"
#include <string>
#include <map>


class TextureManager
{
public:

	TextureManager();
	~TextureManager();


	static void createInstance();
	static TextureManager* getInstance();
	static void deleteInstance();

	void load(const std::string name,const std::string filePath,ImageType t);
	void bind(const std::string name);
	void replace(const std::string name, const GLuint texture);
	
	const Texture &operator[](const std::string&) const;
	const int getWidth(const std::string&) const;
	const int getHeight(const std::string&)const;
	const float getWidthScreensapce(const std::string &) const;
	const float getHeightScreensapce(const std::string &) const;
private:

	std::map<std::string,Texture> textures;
	static TextureManager * instance;
	
};

#endif