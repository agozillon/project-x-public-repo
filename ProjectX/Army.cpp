#include "Army.h"

#include "ShaderManager.h"
#include "RenderManager.h"
#include "MapManager.h"
#include "FactionManager.h"
#include "Mouse.h"

void Army::Render() const
{
	ShaderManager::getInstance()->use("StrategicMap");

	int x = MapManager::getInstance()->GetX();
	int y = MapManager::getInstance()->GetY();
	int zoom = MapManager::getInstance()->GetZoom();
	float left =  x-zoom;
	float right=  x+zoom;	
	float bottom= y-zoom;
	float top  =  y+zoom;
	glm::mat4 projection = glm::ortho(left,right,bottom,top);
	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("projection",1,false,glm::value_ptr(projection));

	glm::mat4 mvp(1.0f);
	mvp = glm::translate(mvp,glm::vec3(m_X,m_Y,0.0f));
	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	RenderManager::getInstance()->renderRenderable("Army");

	glEnable(GL_DEPTH_BUFFER);
}


void Army::HandleMouseClick(const std::pair<unsigned int,unsigned int>& pos)
{
	if(Mouse::getInstance()->getLeftState() == BUTTON_STATE_DOWN )
	{
		if(FactionManager::getInstance()->GetSelectedArmy() != this )
		{
			if( MouseOver(pos) )
			{
				FactionManager::getInstance()->SetSelectedArmy(this);
				printf("Selected\n");
			}
		} else if( pos.first != m_X && pos.second != m_Y )
		{
			if( m_vTilePath.size() == 0 )
			{
				printf("Set Destination %d %d\n",pos.first,pos.second);
				m_vTilePath.push_back( pos  );
			}
		}
	}
}


void Army::OnTurnEnd()
{
	if( m_vTilePath.size() != 0)
	{
		m_X = m_vTilePath.back().first;
		m_Y = m_vTilePath.back().second;
		m_vTilePath.clear();
	}
}

