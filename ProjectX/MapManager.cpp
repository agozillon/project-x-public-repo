#include "MapManager.h"
#include "TileManager.h"
#include "ResourceManager.h"
#include "FactionManager.h"
#include "StrategicMapManager.h"

MapManager * MapManager::instance = nullptr;


MapManager::MapManager()
{
	mode = TERRAIN_MAP;
	m_X = 5;
	m_Y = 5;
	m_Zoom = 1.0f;
}

MapManager::~MapManager()
{}


void MapManager::createInstance()
{
	if( instance ) return;
	instance = new MapManager();
}


void MapManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = nullptr;
}



MapManager * MapManager::getInstance()
{
	if( !instance ) createInstance();
	return instance;
}


void MapManager::switchMode(const MapMode m)
{
	mode = m;
}


void MapManager::draw(const Tile * tile,glm::mat4 & mvp)
{	
	glm::vec4 tint = glm::vec4(1.0f,1.0f,1.0f,1.0f);

	if( mode == TERRAIN_MAP ) 
	{
		ShaderManager::getInstance()->getShader("StrategicMap")->setUniform4fv("tint",1,glm::value_ptr(tint));
		TileManager::getInstance()->draw(tile);
	}
		else if( mode == POLITICAL_MAP )
	{
		if(tile->getType() == WATER ) 
		{
			tint = FactionManager::getInstance()->getColour("NULL");
			ShaderManager::getInstance()->getShader("StrategicMap")->setUniform4fv("tint",1,glm::value_ptr(tint));
			TileManager::getInstance()->draw(tile);
		}
		else 
		{
			tint = FactionManager::getInstance()->getColour(tile->getOwner());
			ShaderManager::getInstance()->getShader("StrategicMap")->setUniform4fv("tint",1,glm::value_ptr(tint));
			RenderManager::getInstance()->renderRenderable("PoliticalTile");
		}
	}
		else if( mode == RESOURCE_MAP )
	{
		tint = glm::vec4(1.0f,1.0f,1.0f,1.0f);
		ShaderManager::getInstance()->getShader("StrategicMap")->setUniform4fv("tint",1,glm::value_ptr(tint));
		TileManager::getInstance()->draw(tile);

		mvp = glm::translate(mvp,glm::vec3(0.0f,0.0f,0.1f));
		ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
		ResourceManager::getInstance()->renderResource(tile);
	}
}



void MapManager::MoveX(int amount)
{ 

	if( m_X - m_Zoom + amount > -1 && m_X + m_Zoom + amount <= StrategicMapManager::getInstance()->GetMap()->getWidth() )
		m_X += amount; 
}
void MapManager::MoveY(int amount) 
{ 

	if( m_Y - m_Zoom + amount > -1 && m_Y + m_Zoom + amount <= StrategicMapManager::getInstance()->GetMap()->getHeight() )
		m_Y += amount; 
}

void MapManager::MoveZoom(int amount)
{
	if( m_Zoom + amount >0 
		&& m_Zoom + m_X + amount <= StrategicMapManager::getInstance()->GetMap()->getWidth() 
		&& -(m_Zoom+amount) + m_X > -1
		&& m_Zoom + m_Y + amount <= StrategicMapManager::getInstance()->getInstance()->GetMap()->getHeight() 
		&& -(m_Zoom+amount) + m_Y > -1 
		) 
			
		m_Zoom += amount; 
}


