#ifndef SHADER_MANAGER_H
#define SHADER_MANAGER_H
#include "ShaderProgram.h"
#include <map>

/*
	This class creates and maintains the shaders/shader programs to be used by the user
*/

class ShaderManager
{
public:
	~ShaderManager();

	static void createInstance();
	static ShaderManager* getInstance();
	static void deleteInstance();

	void use(const std::string& shader);
	ShaderProgram * getShader(const std::string name);

	void initShaderProgram(const std::string programName,const std::string vertShaderName,const std::string fragShaderName, 
							const std::string vertFilePath,const std::string fragFilePath);


	void createProgram(const std::string name);
	void createShader(const std::string name,ShaderType);
	void compileShader(const std::string name);
	void loadShader(const std::string name,const std::string filePath);
	void attachShader(const std::string programName,const std::string shaderName);
	void link(const std::string name); 

	ShaderProgram *  operator[](std::string name);

private:

	ShaderProgram * nullProgram;
	ShaderManager();

	std::map<std::string,ShaderProgram*> shaderPrograms;
	std::map<std::string,Shader*> shaders;
	static ShaderManager *instance;
};

#endif