#ifndef MAP_H
#define MAP_H
#include <vector>
#include "Tile.h"


class Map
{
public:
	Map(std::vector<std::vector<Tile>>* tiles);

	void draw();

	int getHeight();
	int getWidth();


	Tile & getTile(unsigned int x,unsigned int y); // get the tile at position

	void setDimentions(int w,int h);

	//this expects the x and y to be in screenspace coordinates
	std::pair<unsigned int,unsigned int> checkMouseCollision();

	const std::vector<std::vector<Tile>>* getMap();
private:


	//This represents the map
	std::vector<std::vector<Tile>> *map;

	//Width and height of the map
	int width,height;
};

#endif