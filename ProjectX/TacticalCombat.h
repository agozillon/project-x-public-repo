#ifndef TACTICAL_COMBAT_H
#define TACTICAL_COMBAT_H
#include "TacticalMode.h"
#include "WidgetManager.h"

// TacticalCombat is the main mode in the Tactical State and is the battle mode
// as such it continues to render the same things that the TacticalDeployment map
// does minus the Deployment Zone and then adds the functionallity of the 
// two enemy managers and a new widget set to initiate combat and enable
// player and AI control of the units.
class TacticalCombat : public TacticalMode
{
public:
	~TacticalCombat();
	TacticalCombat();
	void draw();
	void update();
	void interaction();
	void setMap(LevelManager * lManager, std::vector<Unit*> pUnits, std::vector<Unit*> eUnits);

private:
	WidgetManager widgetManager;
	LevelManager * levelManager;
	EnemyManager * enemyManager;
	PlayerManager * playerManager;
};
#endif