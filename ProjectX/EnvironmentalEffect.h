#ifndef ENVIRONMENTAL_EFFECT_H
#define ENVIRONMENTAL_EFFECT_H
#include "Particles.h"
#include "BoundingBox.h"
#include <string>
using namespace glm;	

//Abstract interface of the EnvironmentalEffect class family that virtualizes a bunch of functions that every 
// Environmental will use and must create to be instantiated, alongside variable types it will need
class EnvironmentalEffect
{
public:
	virtual const vec3 getPosition() = 0;
	virtual void updatePosition(vec3 pos) = 0;
	virtual void draw(mat4 view, mat4 projection) = 0;
	virtual BoundingBox* getCollisionBox() = 0;

private:
	vec3 position; 
	BoundingBox * collisionBox;
	Particles * indicator;
};

#endif
