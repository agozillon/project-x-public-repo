#version 330

out vec4 colour;
in vec2 UV;

uniform sampler2D textureUnit0;
uniform float tint;
uniform float transparency = 1.0;

void main()
{
	colour = texture2D(textureUnit0,UV)*vec4(vec3(tint),transparency);
}