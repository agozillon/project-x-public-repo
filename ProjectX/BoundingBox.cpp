#include "BoundingBox.h"
#include <iostream>
#include <stack>
#include <vector>
#include <sstream>

// counts the number of BBoxes so we can generate a unique id tag for the RenderManager
// on the fly for every BBox 
int BoundingBox::bCount = 0;

// constructor that accepts a position and a meshes vertices and a count of them and a scalar, from these
// it calculates the AABB size, positions it and scales it relevantly(scalars should be same size as the amount
// you scale the mesh by!)
BoundingBox::BoundingBox(const vec3 pos, const float * meshVertices, const int numberOfVertices, const vec3 scalar): scale(scalar),
	position(pos)
{

	// looping through making sure the values we're checking are positive
	// and finding the largest vertice on the x, y and z to create the bounding box
	for(int i = 0; i < numberOfVertices; i+=3)
	{
		float tempVal;

		if(meshVertices[i] < 0)
			tempVal = -meshVertices[i];
		else
			tempVal = meshVertices[i];

		if(tempVal > dimensions.x)
		   dimensions.x = tempVal;

		if(meshVertices[i+1] < 0)
			tempVal = -meshVertices[i+1];
		else
			tempVal = meshVertices[i+1];

		if(tempVal > dimensions.y)
		   dimensions.y = tempVal;

		if(meshVertices[i+2] < 0)
			tempVal = -meshVertices[i+2];
		else
			tempVal = meshVertices[i+2];

		if(tempVal > dimensions.z)
		   dimensions.z = tempVal;
	}
	
	dimensions = dimensions * scale;

	// THE BELOW IS ALL DATA FOR RENDERING THE BBOXS ESSENTIALLY UNREQUIRED TO OVER ALL FUNCTIONALLIY, LARGELLY FOR TEST PURPOSES
	
	// the cubes index count and vertex count
	GLuint cubeIndexCount = 36;
	const unsigned int vertexCount = 8;
	
	// hard coded indices for the cube
	unsigned short cubeIndices[] = 
	{
		0,1,2, 0,2,3, // back face 
		1,0,5, 0,4,5, // left face
		6,3,2, 3,6,7, // right face
		1,5,6, 1,6,2, // top face
		0,3,4, 3,7,4, // bottom face
		6,5,4, 7,6,4  // front face
	};

	std::vector<float> cubeVertices;
	cubeVertices.resize(24);

	// list of cube vertices being setup alligned like this for ease of reading and understanding 
	// uses the dimensions of the bounding box to create it. 
    cubeVertices[0] = -dimensions.x;  cubeVertices[1] = -dimensions.y;  cubeVertices[2] = -dimensions.z;  // vert 0
	cubeVertices[3] = -dimensions.x;  cubeVertices[4] = dimensions.y;   cubeVertices[5] = -dimensions.z;  // vert 1
	cubeVertices[6] = dimensions.x;   cubeVertices[7] = dimensions.y;   cubeVertices[8] = -dimensions.z;  // vert 2
	cubeVertices[9] = dimensions.x;   cubeVertices[10] = -dimensions.y; cubeVertices[11] = -dimensions.z; // vert 3
	cubeVertices[12] = -dimensions.x; cubeVertices[13] = -dimensions.y; cubeVertices[14] = dimensions.z;  // vert 4
	cubeVertices[15] = -dimensions.x; cubeVertices[16] = dimensions.y;  cubeVertices[17] = dimensions.z;  // vert 5
	cubeVertices[18] = dimensions.x;  cubeVertices[19] = dimensions.y;  cubeVertices[20] = dimensions.z;  // vert 6
	cubeVertices[21] = dimensions.x;  cubeVertices[22] = -dimensions.y; cubeVertices[23] = dimensions.z;  // vert 7

	bCount++; 
	std::ostringstream s;
	name = "bounding";
	s << name << bCount;	
	std::string temp(s.str());
	name = temp;
	RenderManager::getInstance()->addRenderable(name,new Mesh(&cubeVertices[0], cubeVertices.size(), NULL, 0, cubeIndices, cubeIndexCount, NULL, 0)); 
}

// basic constructor that accepts the BoundingBox's position and dimensions and sets them up, USED IN RT3D of Andrew Gozillon
BoundingBox::BoundingBox(const glm::vec3 bboxPositions, const glm::vec3 bboxDimensions)
{
	dimensions.x = bboxDimensions.x;
	dimensions.y = bboxDimensions.y;
	dimensions.z = bboxDimensions.z;

	position.x = bboxPositions.x;
	position.y = bboxPositions.y;
	position.z = bboxPositions.z;

	// THE BELOW IS ALL DATA FOR RENDERING THE BBOXS ESSENTIALLY UNREQUIRED TO OVER ALL FUNCTIONALLIY, LARGELLY FOR TEST PURPOSES
	
	// the cubes index count and vertex count
	GLuint cubeIndexCount = 36;
	const unsigned int vertexCount = 8;
	
	// hard coded indices for the cube
	unsigned short cubeIndices[] = 
	{
		0,1,2, 0,2,3, // back face 
		1,0,5, 0,4,5, // left face
		6,3,2, 3,6,7, // right face
		1,5,6, 1,6,2, // top face
		0,3,4, 3,7,4, // bottom face
		6,5,4, 7,6,4  // front face
	}; 

	std::vector<float> cubeVertices;
	cubeVertices.resize(24);

	// list of cube vertices being setup alligned like this for ease of reading and understanding 
	// uses the dimensions of the bounding box to create it
    cubeVertices[0] = -dimensions.x;  cubeVertices[1] = -dimensions.y;  cubeVertices[2] = -dimensions.z;  // vert 0
	cubeVertices[3] = -dimensions.x;  cubeVertices[4] = dimensions.y;   cubeVertices[5] = -dimensions.z;  // vert 1
	cubeVertices[6] = dimensions.x;   cubeVertices[7] = dimensions.y;   cubeVertices[8] = -dimensions.z;  // vert 2
	cubeVertices[9] = dimensions.x;   cubeVertices[10] = -dimensions.y; cubeVertices[11] = -dimensions.z; // vert 3
	cubeVertices[12] = -dimensions.x; cubeVertices[13] = -dimensions.y; cubeVertices[14] = dimensions.z;  // vert 4
	cubeVertices[15] = -dimensions.x; cubeVertices[16] = dimensions.y;  cubeVertices[17] = dimensions.z;  // vert 5
	cubeVertices[18] = dimensions.x;  cubeVertices[19] = dimensions.y;  cubeVertices[20] = dimensions.z;  // vert 6
	cubeVertices[21] = dimensions.x;  cubeVertices[22] = -dimensions.y; cubeVertices[23] = dimensions.z;  // vert 7

	bCount++; 
	std::ostringstream s;
	name = "bounding";
	s << name << bCount;	
	std::string temp(s.str());
	name = temp;
	RenderManager::getInstance()->addRenderable(name,new Mesh(&cubeVertices[0], cubeVertices.size(), NULL, 0, cubeIndices, cubeIndexCount, NULL, 0)); 
}

// function primarily for testing it draws the BoundingBox on screen 
void BoundingBox::draw(mat4 viewProjection)
{
	std::stack<mat4> mStack;
	mStack.push(mat4(1.0));
	mStack.top() = glm::translate(mStack.top(), position);

	mat4 temp = viewProjection * mStack.top();
	ShaderManager::getInstance()->getShader("simpleTac")->setUniformMatrix4fv("MVP", 1, false, value_ptr(temp));  //set the MVP
	RenderManager::getInstance()->renderRenderable(name);
}

// function that accepts a pointer to a BoundingBox and checks for a collision between the current bounding box
// and the passed in BoundingBox and then returns either true or false depending on if they've colided
// USED IN RT3D of Andrew Gozillon
const bool BoundingBox::detectBoxCollision(BoundingBox * colideeBoundingBox)
{
	// really simple Axis alligned bounding box detection checks if a collision is actually possible for each side
	// and if its not then a collision can't be happening so it exits early with a false value if it gets through them all
	// correctly then it returns true
	if (position.x + dimensions.x < colideeBoundingBox->getPosition().x - colideeBoundingBox->getDimensions().x)
		return false;

	if (position.x - dimensions.x > colideeBoundingBox->getPosition().x + colideeBoundingBox->getDimensions().x)
		return false;

	if (position.y + dimensions.y < colideeBoundingBox->getPosition().y - colideeBoundingBox->getDimensions().y)
		return false;

	if (position.y - dimensions.y > colideeBoundingBox->getPosition().y + colideeBoundingBox->getDimensions().y)
		return false;

	if (position.z + dimensions.z < colideeBoundingBox->getPosition().z - colideeBoundingBox->getDimensions().z)
		return false;

	if (position.z - dimensions.z > colideeBoundingBox->getPosition().z + colideeBoundingBox->getDimensions().z)
		return false;

	// returns the bool 
	return true;
}

// simple function that calculates the distance needed to seperate to bounding boxes if they've entered each other
// during intersection, can happen during the interpolation phase of the traversingPath function and can cause 
// AI units to permanently stick to an obstacle (rarely if at all happens to player controlled units).
const vec3 BoundingBox::seperationDistance(BoundingBox * collider)  
{
	glm::vec3 seperationDist;
	// if the colliders position is greater than position x on the bounding box then its on the "right" side of the
	// collision box so we need to use the right side of the colidee and the left side of the colliders box and then negate the distance
	// to calculate the distance requried to seperate them. The 1.1f is just to increase the seperation distance a little so its not right on the edge.
	if(collider->getPosition().x > position.x)
	{
		seperationDist.x = ((position.x + dimensions.x) - (collider->getPosition().x - collider->getDimensions().x)) * 1.1f;
	}
	else 
	{
		seperationDist.x = ((position.x - dimensions.x) - (collider->getPosition().x + collider->getDimensions().x)) * 1.1f;
	}

	// same as above except for with the Y
	if(collider->getPosition().y + collider->getDimensions().y > position.y)
	{
		seperationDist.y = ((position.y + dimensions.y) - (collider->getPosition().y - collider->getDimensions().y)) * 1.1f;
	}
	else
	{
		seperationDist.y = ((position.y - dimensions.y) - (collider->getPosition().y + collider->getDimensions().y)) * 1.1f;
	}

	// same as above except for with the Z
	if(collider->getPosition().z + collider->getDimensions().z > position.z)
	{
		seperationDist.z = ((position.z + dimensions.z) - (collider->getPosition().z - collider->getDimensions().z)) * 1.1f;
	}
	else
	{
		seperationDist.z = ((position.z - dimensions.z) - (collider->getPosition().z + collider->getDimensions().z)) * 1.1f;
	}

	return seperationDist;
}

// 3d ray collision check, had a lot of help from online sources(since my maths isn't great) however it
// calculates where a ray line intersects the 6 planes of the bounding box (represented as a line)
const bool BoundingBox::detectRayCollision(const glm::vec3 rayPoint, const glm::vec3 rayDirection, glm::vec3 &collisionPoint)
{
	bool collision = true;
	
	glm::vec3 max, min;
	glm::vec3 bMax, bMin;


	// calculating min/max of bboxs x, y and z dimensions each of these max and min values
	// creates our bounding box planes for that specific dimension
	bMax.x = position.x + dimensions.x; bMin.x = position.x - dimensions.x;
	bMax.y = position.y + dimensions.y;	bMin.y = position.y - dimensions.y;
	bMax.z = position.z + dimensions.z; bMin.z = position.z - dimensions.z;
	
	// calculating where on the Bounding Boxs X plane the ray interects
	// the min 
	if(rayDirection.x >= 0)
	{
		// solving for 2 lines bMax is one rayPoint is one, we get the direction
		// from the rays origin to the bounding boxs max x and then divide it by
		// the rays direction to see where it crosses that plane at a -ve value
		// means it's behind the rays origin so the ray intersected it some point "in the past"
		// another way of looking at it (my prefferred way for visualizing it at least so may not be
		// accurate) is that we're getting the number of increments of the rays direction
		// to get to the bounding boxes minimum and maximum ranges I.E ray direction x at 0.5
		// box x min range = 2, 2 / 0.5 = 4 thus it takes 4 increments of the ray to get here
		max.x = (bMax.x - rayPoint.x) / rayDirection.x;
		min.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}
	else
	{
		min.x = (bMax.x - rayPoint.x) / rayDirection.x;
		max.x = (bMin.x - rayPoint.x) / rayDirection.x;
	}

	if(rayDirection.y >= 0)
	{
		max.y = (bMax.y - rayPoint.y) / rayDirection.y;
		min.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
	else
	{
		min.y = (bMax.y - rayPoint.y) / rayDirection.y;
		max.y = (bMin.y - rayPoint.y) / rayDirection.y;
	}
		
	// basically if the rays min.x intersect point is greater than
	// the rays max y intersect point or min.y is greater than max.x 
	// then it doesn't intersect as the ray is travelling between two planes
	// that never allow for a connection within the object I.E overshoots
	// min x should be lower than max y and min y should be lower than max x 
	// for a ray to travel between them!
	// My visualized (and perhaps inaccurate) way of looking at it is that 
	// if the minimum x's increments are greater than the maximum y's increments
	// then the ray overshoots / goes above our box e.g min x increments = 10 to get
	// to the desired plane/location and max y increments = 8 to get to its desired plane
	// box min x = 1 box max y = 2, ray x = 0.10 ray y = 0.25, 10 * 0.10 = 1 y with 10 increments
	// = 2.50! so when we just reach the minimum x we've overshot the maximum y by 2 increments!
	if(min.x > max.y || min.y > max.x)
		return false;

	// these have no bearing what so ever on the check
	// it's essentially our min and max scale value to scale the ray direction with
	// to help get the intersection point. we want the smallest number and largest of the increments
	//to increment our ray to get the very first touch of the cube instead of too far in etc
	if(min.y > min.x)
		min.x = min.y;

	if(max.y < max.x)
		max.x = max.y;

	// calculating a z plane using the min and max, checks if the ray
	// is greater or smaller as this will change which is the min and which is the max
	// of the bounding box in relation to the rays direction
	if(rayDirection.z >= 0)
	{
		max.z = (bMax.z - rayPoint.z) / rayDirection.z;
		min.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}
	else
	{
		min.z = (bMax.z - rayPoint.z) / rayDirection.z;
		max.z = (bMin.z - rayPoint.z) / rayDirection.z;
	}

	if(min.x > max.z || min.z > max.x)
		return false;

	if(min.z > min.x)
		min.x = min.z;

	if(max.z < max.x)
		max.x = max.z;

	// rays origin point + the number of ray increments to the intersect point
	// multiplied by our rays direction in all cases
	collisionPoint =  rayPoint + (min.x * rayDirection);
	
	return collision;
}


