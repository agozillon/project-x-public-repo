#include "Map.h"
#include "RenderManager.h"
#include "MapManager.h"
#include "CameraManager.h"
#include "Mouse.h"
#include "GameManager.h"



Map::Map(std::vector<std::vector<Tile>>*tiles)
{
	map = tiles;
	width = tiles->size();
	height = (*tiles)[0].size();
}


int Map::getHeight()
{
	return height;
}

int Map::getWidth()
{
	return width;
}

void Map::setDimentions(int w,int h)
{
	width = w;
	height = h;
}

void Map::draw()
{	
	glDepthMask(GL_FALSE);
	ShaderManager::getInstance()->use("StrategicMap");

	int x = MapManager::getInstance()->GetX();
	int y = MapManager::getInstance()->GetY();
	int zoom = MapManager::getInstance()->GetZoom();

	float left =  x-zoom;
	float right=  x+zoom;	
	float bottom= y-zoom ;
	float top  =  y+zoom;

	glm::mat4 projection = glm::ortho(left,right,bottom,top);
	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("projection",1,false,glm::value_ptr(projection));


	for(int y =height-1;y >= 0; y--)
	{
		for(int x = 0;x != width; x++)
		{	

			if( !(x < left || x >= right || y >= top || y < bottom))
			{
				glm::mat4 mvp(1.0f);
				mvp = glm::translate(mvp,glm::vec3(x,y,0.0f));

				ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));

				MapManager::getInstance()->draw(&(*map)[x][y],mvp);
			}
		}
	}
	glDepthMask(GL_TRUE);

}

const std::vector<std::vector<Tile>> * Map::getMap()
{
	return map;
}


std::pair<unsigned int,unsigned int> Map::checkMouseCollision()
{
	std::pair<unsigned int, unsigned int> returnee(width,height);


	float x = (float)Mouse::getInstance()->getX();  // (x/(width/2)) - 1
	float y = (float)Mouse::getInstance()->getY();// (y/(height/2)) -1

	if( x > 0 && x < GameManager::getInstance()->getWidth() && y > 0 && y < GameManager::getInstance()->getHeight() )
	{
	
		int zoom = MapManager::getInstance()->GetZoom();

		int width = GameManager::getInstance()->getWidth()/(zoom*2);	
		int height = GameManager::getInstance()->getHeight()/(zoom*2);

		float left=  MapManager::getInstance()->GetX()-zoom;	
		float bottom=MapManager::getInstance()->GetY()-zoom;
		
		int ix = (x/width) + left;
		int iy = (y/height)+ bottom;
	
		returnee= std::pair<unsigned int,unsigned int>(ix,iy);
	}

	return returnee;
	
}


Tile & Map::getTile(unsigned int x,unsigned int y)
{
	return (*map)[x][y];
}