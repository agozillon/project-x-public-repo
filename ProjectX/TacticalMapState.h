#ifndef TACTICAL_MAP_STATE_H
#define TACTICAL_MAP_STATE_H
#include "GameState.h"
#include "TacticalMapManager.h"

// This is the Tactical Map State class that essentially holds the TacticalMapManager
// and calls its draw and update functions and check for input functions! So it essentially
// just adds another level of abstraction to the system rather than having all the code within
// the TacticalMapState
class TacticalMapState: public GameState
{
public:
	TacticalMapState();
	~TacticalMapState();

	void draw();
	void update();
	void onExit() { }
	void onEntry();

private:

	void checkInput();
};
#endif