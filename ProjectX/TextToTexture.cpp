#include "TextToTexture.h"
#include <iostream>   // for console output
#include "TextureManager.h"

// constructor that takes in a TTF_Font name and opens it and assigns it to textFont,
// takes an int in for the size of font and a string for the texture name, not an existing 
// texture, just a blank name for the TextToTexture to know where to render it to! 
TextToTexture::TextToTexture(std::string fontName, int fontSize, std::string textureToRenderName)
{	
	// Opening text font, having one open for each TextToTexture may
	// end up being a bit memory intensive, possible future optimization
	textFont = TTF_OpenFont(fontName.c_str(), fontSize);
	
	texture = textureToRenderName;
	// preload an empty texture with the TextToTextures name to keep a place for it
	// as we will be rendering to it when createTextTexture is called
	TextureManager::getInstance()->load(textureToRenderName.c_str(),"null_texture.bmp",BMP);
}

// destructor for deleting instantiated objects and in this case closing 
// the textFont
TextToTexture::~TextToTexture()
{
	TTF_CloseFont(textFont);
}

// function that takes in a set of chars and 3 ints representing RGB and then creates a
// texture version of the text and returns it
void TextToTexture::createTextTexture(const char * str, SDL_Color colour) 
{	
	if (textFont == NULL)
	{
		std::cout << "Failed to open font" << std::endl;
	}

	SDL_Color bg = { 0, 0, 0 };

	// creates an SDL surface of the texture
	SDL_Surface *stringImage;
	stringImage = TTF_RenderText_Blended(textFont,str,colour);

	if (stringImage == NULL)
	{
		std::cout << "String surface not created." << std::endl;
	}

	// converts the texture format to RGB or RGBA based on
	// if it has 4 colours or not and reverses the RGB based
	// on its internal format
	GLuint w = stringImage->w;
	GLuint h = stringImage->h;
	GLuint colours = stringImage->format->BytesPerPixel;

	GLuint format, internalFormat;
	
	if (colours == 4)  // alpha
	{  
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGBA;
		}
	    else
		{
		    format = GL_BGRA;
		}
	} 
	else // no alpha
	{             
		if (stringImage->format->Rmask == 0x000000ff)
		{
			format = GL_RGB;
		}
	    else
		{
		    format = GL_BGR;
		}
	}

	internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

	// now create the OpenGL texture from the SDL surface!
	GLuint tmpID;

	glGenTextures(1, &tmpID);
	glBindTexture(GL_TEXTURE_2D, tmpID);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, w, h, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

	// replace the previous texture, it also cleans up the texture on the GPU
	TextureManager::getInstance()->replace(texture, tmpID);

	// SDL surface was used to generate the texture but is no longer
	// required. Release it to free memory
	SDL_FreeSurface(stringImage);

}
