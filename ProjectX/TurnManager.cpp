#include "TurnManager.h"
#include "StrategicGameObject.h"
#include "FactionManager.h"
TurnManager * g_pTurnManager = nullptr;

void TurnManager::EndTurn()
{

	FactionManager::getInstance()->SetSelectedArmy(0);

	for(unsigned iObject = 0; iObject < m_vpGameObjects.size(); iObject++)
	{
		m_vpGameObjects[iObject]->OnTurnEnd();
	}
	m_TurnNumber++;


	printf("\nTurn %i\n\n",m_TurnNumber);
}

