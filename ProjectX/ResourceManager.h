#ifndef RESOURCE_MANAGER_H
#define RESOURCE_MANAGER_H
#include "Tile.h"

enum ResourceType
{
    NULL_RESOURCE = 0,
	TIMBER		  = 1,
	STONE         = 2,
	IRON          = 3,
	COAL          = 4,


	NUM_RESOURCES = 4 //exclude the null resource 
};


class ResourceManager
{
public:

	static ResourceManager * getInstance();
	static void createInstance();
	static void deleteInstance();

	void renderResource(const Tile*);
	void renderResource(const ResourceType);

	void renderResourceIcon(const ResourceType);
private:

	static ResourceManager * instance;
	ResourceManager();
	~ResourceManager();

};

#endif