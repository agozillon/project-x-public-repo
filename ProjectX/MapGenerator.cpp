#include "MapGenerator.h"
#include <iostream>
#include "FactionManager.h"

Map * MapGenerator::generateMap(const  int w,const  int h,const  int factionCount,MapType type)
{
	for(unsigned int i =1; i < factionCount; i++) //for each faction excluding null
	{
		FactionManager::getInstance()->getFaction(i)->ClearTiles();

	}


	tiles = new std::vector<std::vector<Tile>>();
	
	width = w;
	height = h;

	tiles->resize(width); 
	for(unsigned int i = 0; i < tiles->size(); i++)
	{
		(*tiles)[i].resize(height);
	}


	// Populate the world with blank water tiles
	for( unsigned int x = 0; x < width; x++)
	{
		for( int y = 0; y < height; y++)
		{
			(*tiles)[x][y] = Tile(0,0,0,0);
		}
	}
	
	std::vector<std::pair<TILE_XY, int>> seedTiles = getSeedTiles(type); //the 'seed' tiles and their respective elevations are what we work out the remaining landmasses from

	for( unsigned int i =0; i < seedTiles.size(); i++) //turn the individual lone tile islands into proper clumps
	{
		int x = seedTiles[i].first.first;
		int y = seedTiles[i].first.second;
		spawnTilesAround(x,y,seedTiles[i].second,CENTER); //spawn tiles around this one
	}


	for(unsigned int i = 0; i < 30; i++)
	{
		TILE_XY randomTile = landTiles[ (rand()%landTiles.size()) ];

		(*tiles)[randomTile.first][randomTile.second].setResource( (rand()%2 +1));
		(*tiles)[randomTile.first][randomTile.second].setResourceAmount(20);
	}

//	removeLoners();

	for(unsigned int i =1; i < factionCount; i++) //for each faction excluding null
	{
		if(!FactionManager::getInstance()->getFaction(i)->IsInitalized() ) 
			FactionManager::getInstance()->getFaction(i)->Init();

		TILE_XY randomTile;
		
		while(true)
		{	
			randomTile = landTiles[ (rand() %landTiles.size())]; //pick a random tile
			
			bool tooClose = false; 
			for(unsigned int j = 0; j < factionTiles.size(); j++) // make sure it isn't too close to the other factions
			{
				if( (randomTile.first - factionTiles[j].first > -factionDomainSize*2 && randomTile.first - factionTiles[j].first < factionDomainSize*2) &&
					(randomTile.second -factionTiles[j].second> -factionDomainSize*2 && randomTile.second- factionTiles[j].second< factionDomainSize*2) )
					tooClose = true;
			}

			if( !tooClose ) break; //if the faction is far enough away from the others, break from loop
		}

		factionTiles.push_back(randomTile);

		if( i == 1 )
			FactionManager::getInstance()->getFaction(i)->AddArmy(Army("First Army",randomTile.first,randomTile.second));

		setFactionFromPoint(randomTile.first,randomTile.second,factionDomainSize,i,CENTER);//from this individual point set the surrounding area to be under that faction's control
	}
	





	seedTiles.clear();
	landTiles.clear();
	factionTiles.clear();

	return new Map(tiles);
}



std::vector<std::pair<TILE_XY, int>> MapGenerator::getSeedTiles(MapType type)
{
	std::vector<std::pair<TILE_XY, int>> seedTiles;

	int numberOfPoints; //the number of seed points to spawn
	int rangeX = 0,
		rangeY = 0,
		rangeWidth = width-1,
		rangeHeight= height-1; //the box in which to spawn the seed points 
	int minDistance = 0;
	int elevationFloor = 750,elevationRoof = 250;
	int exclusionZoneX =0, exclusionZoneY = 0;
	int exclusionZoneWidth =0, exclusionZoneHeight = 0;

	factionDomainSize = 5;

	if( type == SMALL_ISLANDS)  
	{
	//	minDistance = 15;
		seaLevel = 750;
		numberOfPoints = width*1.75;
	}
		else if( type == LANDMASS )
	{
		rangeX = width/5;
		rangeY = height/5;
		rangeWidth = (width - width/5) - rangeX; // as random gen wil go from rangeX -> (rangeWidth+rangeX)
		rangeHeight = (height- height/5)- rangeX;
		seaLevel = 600;
		numberOfPoints = width;

	}
	 else if( type == LARGE_ISLANDS )
	{
		seaLevel = 600;
		numberOfPoints = width/2.5;
	} else if( type == CONTINENTS )
	{
		rangeX = width/10;
		rangeY = height/10;
		rangeWidth = (width - width/10) - rangeX; // as random gen wil go from rangeX -> (rangeWidth+rangeX)
		rangeHeight = (height- height/10)- rangeX;

		seaLevel = 500;
		minDistance = 10;
		numberOfPoints = width/5;
	}else if (type == MEDITERRANEAN )
	{
		exclusionZoneX = width/5;
		exclusionZoneY = height/5;
		exclusionZoneWidth =(width-width/5) - exclusionZoneX;
		exclusionZoneHeight=(height-height/5)-exclusionZoneX;
		seaLevel = 700;
		numberOfPoints = width*7.5;
	}


	seaLevel = seaLevel > 1000 ? 1000 : seaLevel; //clamp within the bounds of 0-1
	seaLevel = seaLevel < 0 ? 0 : seaLevel;

	for( unsigned int i =0; i < numberOfPoints; i++)
	{

		int randomX,randomY;
		while(true)
		{	
			randomX = rand()%rangeWidth + rangeX;
			randomY = rand()%rangeHeight+ rangeX; 


			if( (*tiles)[randomX][randomY].getType() == GRASS ) //if this tile has already been initialised, ignore and re-loop
			{
				i--;
				continue;
			}	



			if( exclusionZoneWidth != 0) //if there is an exclusion zone inplace
			{
				if( (randomX > exclusionZoneX && randomX < exclusionZoneX + exclusionZoneWidth) &&  //if this point is in the zone, find a new point
					(randomY > exclusionZoneY && randomY < exclusionZoneY + exclusionZoneHeight))
				{
					i--;
					continue;
				}		
			}


			bool tooClose = false; //check if the point is sufficently far away from the others
			for(unsigned int j = 0; minDistance != 0 && j < seedTiles.size(); j++)
			{
				if( (randomX - seedTiles[j].first.first > -minDistance && randomX - seedTiles[j].first.first < minDistance) &&
					(randomY - seedTiles[j].first.second> -minDistance && randomY - seedTiles[j].first.second<minDistance) )
					tooClose = true;
			}

			if( !tooClose ) break; //if the tile is sufficiently far away from the other tiles
		}
		
		int elevation = (rand()%elevationFloor + elevationRoof);//generate random number in within the set range to be the starting elevation
		seedTiles.push_back( std::pair<TILE_XY,int>( TILE_XY(randomX,randomY),elevation));
		
	}

	return seedTiles;
}




void MapGenerator::spawnTilesAround(const int x,const int y,const int elevation,Direction directionFrom)
{
	if( seaLevel >= elevation ) return; //if small enough to be considered water
	if( x >= width || y >=height || x < 0 || y < 0 ) return; //or outwidth the bounds of the map

	
	if( (*tiles)[x][y].getType() != GRASS )
	{
		(*tiles)[x][y].setType(GRASS);
		landTiles.push_back( TILE_XY(x,y) );
	}

	if( directionFrom != RIGHT) spawnTilesAround(x-1,y,elevation -(rand()%100),LEFT); //left
	if( directionFrom != LEFT) spawnTilesAround(x+1,y, elevation - (rand()%100),RIGHT); //right

	if( directionFrom != DOWN ) spawnTilesAround(x,y+1,elevation - (rand()%100),UP); // above 
	if( directionFrom != UP   ) spawnTilesAround(x,y-1,elevation - (rand()%100),DOWN); //below



}


void MapGenerator::setFactionFromPoint(const int x,const int y,const  int distance,const  int factionIndex,Direction directionFrom)
{
	if( distance <= 0 ) return; //if small enough to be considered water
	if( x >= width || y >= height || x < 0 || y < 0 ) return; //or outwidth the bounds of the map

	if( (*tiles)[x][y].getType() != WATER && directionFrom != CENTER)
	{
		(*tiles)[x][y].setOwner(factionIndex);

		if( (*tiles)[x][y].getResourceType() != 0 )
		{
			FactionManager::getInstance()->getFaction(factionIndex)->AddResourceTile(x,y);
		}

	}

	if( directionFrom != DOWN )  setFactionFromPoint(x,y+1,distance-1,factionIndex,UP); // up
	if( directionFrom != UP )    setFactionFromPoint(x,y-1,distance-1,factionIndex,DOWN); // down

	if( directionFrom != LEFT )  setFactionFromPoint(x+1,y,distance-1,factionIndex,RIGHT); // Right
	if( directionFrom != RIGHT ) setFactionFromPoint(x-1,y,distance-1,factionIndex,LEFT); // Left
	
	if( directionFrom != BOTTOM_LEFT)  setFactionFromPoint(x+1,y+1,distance-1,factionIndex,UPPER_RIGHT); //Upper-Right
	if( directionFrom != BOTTOM_RIGHT) setFactionFromPoint(x-1,y+1,distance-1,factionIndex,UPPER_LEFT); //Upper-left

	if( directionFrom != UPPER_LEFT)  setFactionFromPoint(x+1,y-1,distance-1,factionIndex,BOTTOM_RIGHT); //Bottom-Right
	if( directionFrom != UPPER_RIGHT) setFactionFromPoint(x-1,y-1,distance-1,factionIndex,BOTTOM_LEFT); //Bottom-left
}


std::vector<float> MapGenerator::generateHeightMap(const  int width,const  int height)
{
	std::vector<float> map(width *height);

	float corners[4]; // the four corners of the initial map

	corners[0] = rand()/(float)RAND_MAX;
	corners[1] = rand()/(float)RAND_MAX;
	corners[2] = rand()/(float)RAND_MAX;
	corners[3] = rand()/(float)RAND_MAX;
	
	int lengthX = width,lenghtY = width;

	
	
	return map;
}


void MapGenerator::removeLoners()
{

	for(int i = landTiles.size()-1; i >= 0; i--)
	{
		int x = landTiles[i].first;
		int y = landTiles[i].second;
	

		if( x + 1 != width && (*tiles)[x+1][y].getType() == GRASS ) //check right
			continue;
		if( x - 1 > 0 && (*tiles)[x-1][y].getType() == GRASS ) //check left
			continue;
		if( y + 1 != width && (*tiles)[x][y+1].getType() == GRASS ) //check up
			continue;
		if( y - 1 > 0 && (*tiles)[x][y-1].getType() == GRASS )//check down
			continue;
		if( x +1 != width && y + 1 != height && (*tiles)[x+1][y+1].getType() == GRASS) //check top right
			continue;
		if( x -1 != width && y + 1 != height && (*tiles)[x-1][y+1].getType() == GRASS) //check top left
			continue;
		if( x +1 != width && y - 1 != height && (*tiles)[x+1][y-1].getType() == GRASS) //check bottom right
			continue;
		if( x -1 != width && y - 1 != height && (*tiles)[x-1][y-1].getType() == GRASS) //check bottom left
			continue;


		//if all these checks fail the tile is alone in the ocean
		landTiles.erase(landTiles.begin() +i);
	}
}