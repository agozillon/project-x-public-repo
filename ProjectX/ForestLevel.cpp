#include "ForestLevel.h"
#include "RenderManager.h"
#include "CameraManager.h"
#include "GameManager.h"
#include "Mouse.h"


ForestLevel::ForestLevel()
{	

	// deployment zones for positioning troops in on this level
	playerDeploymentZone = new BoundingBox(glm::vec3(-20, -5, -4), glm::vec3(5, 20, 1.0));
	enemyDeploymentZone = new BoundingBox(glm::vec3(10, -5, -4), glm::vec3(5, 20, 1.0));
	
	// damage effects
	damageEffectList.push_back(new DamageEnvironmentalEffect(1, glm::vec3(0,-5,-3), glm::vec3(2, 2, 2), "DamageParticle", "Particles", 100, glm::vec2(GameManager::getInstance()->getWidth(), GameManager::getInstance()->getHeight())));
	
	// slow effects
	slowEffectList.push_back(new SlowEnvironmentalEffect(0.5, glm::vec3(-9, 9, -3), glm::vec3(7, 6, 2), "SlowParticle", "Particles", 100, glm::vec2(GameManager::getInstance()->getWidth(), GameManager::getInstance()->getHeight())));

	// log
	sceneryList.push_back(new Scenery(glm::vec3(5, 0, -4.5), glm::vec3(45, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.5, 1.0, 1.0), "Log", "Log", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(10, -10, -5), glm::vec3(90, 45, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.8, 1.0, 1.0), "Log", "Log", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-7, -7, -5), glm::vec3(90, -45, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.8, 1.0, 1.0), "Log", "Log", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-18, -16, -5), glm::vec3(90, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 1.0, 1.0), "Log", "Log", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-18, 2, -5), glm::vec3(90, 25, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.8, 1.0, 1.0), "Log", "Log", "simpleTac", true, false));
	
	// tiled floor, unforutnately can't just repeat draw them otherwise picking wouldn't work
	// maybe making a texture(couldn't source one) that can stretch across a huge cube
	// and not look like one patch of grass would be more optimal
	for(int x = -2; x < 2; x++)
	{
		for(int y = -2; y < 2; y++)
		{
			sceneryList.push_back(new Scenery(glm::vec3(x*10, y*10, -5), glm::vec3(0, 0, 0), glm::vec3(5.0, 5.0, 0.02), "Cube", "RealGrass", "simpleTac", false, true));	
		}
	}

	// arbol trees
	sceneryList.push_back(new Scenery(glm::vec3(-12, 14, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-11, 11, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-12, 5, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-2, 8, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-10, -15, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(4, -14, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.3, 0.3, 1.0), "Tree1", "Tree1", "simpleTac", true, false));

	// Pino Pine tree
	sceneryList.push_back(new Scenery(glm::vec3(-8, 12, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 1.0), "Tree2", "Tree2", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-6, 8, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 1.0), "Tree2", "Tree2", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-8, 2, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 1.0), "Tree2", "Tree2", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-18, 12, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 1.0), "Tree2", "Tree2", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -22, -5), glm::vec3(0, 0, 0), glm::vec3(1.0, 1.0, 1.0), glm::vec3(0.2, 0.2, 1.0), "Tree2", "Tree2", "simpleTac", true, false));

	// Pine tree
	sceneryList.push_back(new Scenery(glm::vec3(-15, 10, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), "Tree3", "Tree3", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-4, 4, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), "Tree3", "Tree3", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -16, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), "Tree3", "Tree3", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(4, -20, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), "Tree3", "Tree3", "simpleTac", true, false));

	// huge tree
	sceneryList.push_back(new Scenery(glm::vec3(-10, 8, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), glm::vec3(0.2, 0.2, 1.0), "Tree4", "Tree4", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-4, 14, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), glm::vec3(0.2, 0.2, 1.0), "Tree4", "Tree4", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-4, -10, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), glm::vec3(0.2, 0.2, 1.0), "Tree4", "Tree4", "simpleTac", true, false));
	sceneryList.push_back(new Scenery(glm::vec3(-12, -22, -5), glm::vec3(0, 0, 0), glm::vec3(0.20, 0.20, 0.20), glm::vec3(0.2, 0.2, 1.0), "Tree4", "Tree4", "simpleTac", true, false));
}

// clean up all the levels objects on destruction
ForestLevel::~ForestLevel()
{
	for(size_t i = 0; i < sceneryList.size(); i++)
		delete sceneryList[i];

	for(size_t i = 0; i < damageEffectList.size(); i++)
		delete damageEffectList[i];

	for(size_t i = 0; i < slowEffectList.size(); i++)
		delete slowEffectList[i];

	delete enemyDeploymentZone;
	delete playerDeploymentZone;
}

// initializes elements of the level that need re-initialized after construction
// I.E moving elements being re-positioned etc.
void ForestLevel::init()
{

}

// Render the viewable level elements
void ForestLevel::draw(glm::mat4 viewProjection)
{
	
	for(size_t i = 0; i < sceneryList.size(); i++)
	{
		sceneryList[i]->draw(viewProjection);
	}

	for(size_t i = 0; i < damageEffectList.size(); i++)
		damageEffectList[i]->draw(CameraManager::getInstance()->getLookAt(), CameraManager::getInstance()->getProjection());
	
	for(size_t i = 0; i < slowEffectList.size(); i++)
		slowEffectList[i]->draw(CameraManager::getInstance()->getLookAt(), CameraManager::getInstance()->getProjection());
}
	
// update moving elements of the map (currently no moving elements, particles are implemented
// in a way that they update within the draw method)
void ForestLevel::update()
{
}
	

