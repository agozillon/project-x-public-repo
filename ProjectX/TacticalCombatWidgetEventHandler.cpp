#include "TacticalCombatWidgetEventHandler.h"
#include "TacticalMapManager.h"

// On left click it essentially checks if its a "Card" and then works out which unit
// the card represents from the name and then changes the playerManagers playerSelected
// variable to point to it.
void TacticalCombatWidgetEventHandler::handleMouseLeftClick(Widget *widget)
{

	// comparing strings to see if they're equal to each other and if they've not been
	// pressed recently, only check the first 4 letters, the last "letter" is saved for the
	// units position in the vector
	if(strncmp(widget->getName().c_str(), "Card", 4) == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() ){
		// get the integer at the end of the widgets name
		std::string tmpString;
		for(size_t i = 4; i < widget->getName().size(); i++)
			tmpString += widget->getName()[i];
		// convert it to an int
		int i = atoi(tmpString.c_str());

		// assign the relevant unit to the current PlayerManagers playerSelected variable
		// allowing the player to command the newly selected unit
		playerManager->updatePlayerSelected(playerUnits[i]);
	}

}
