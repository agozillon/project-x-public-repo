// Vertex Shader � file "particle.vert"

#version 330

uniform mat4 MV; // model view
uniform mat4 P; // projection 
uniform vec2 screenSize; 
uniform float particleSize;

layout(location = 0) in vec3 in_Position;

void main(void)
{
	vec4 cameraPos = MV * vec4(in_Position, 1.0);
	
	// The following is an Attenuation calculation for the Particles to make them shrink
	// as they move further from the camera (as point sprites remain the same size and the OpenGL attenuation
	// option is deprecated), had some help online for it
	
	// gets the projection of the particle when its "Centered" on the camera but uses
	// appropriate camera distance and depth ( as we wish to use it to calculate a rough estimate
	// of how big/small particles should be based on the distance from the camera) 

	vec4 projParticleSize = P * vec4(particleSize, particleSize, cameraPos.z, cameraPos.w);

	// perspective dividing the projected point size by its w which gets the x and y values (width and height or just our point sprites
	// size) effected by distance from the camera and the FOV (as W is a combination of near/far and fov values after being multiplied by P)
	// within a -1, 1 space (Normalize Device Co-ordinates I.E NDC space) for each value which dictates the distance it is from the camera and thus
	// where it is on screen (or in this case how big it is on screen). We then use that as a ratio and multiply them by the screen size 
	// to get its size on screen skewed by distance.
	
	vec2 projRatio = screenSize * projParticleSize.xy / projParticleSize.w;

	// then we add the two values up (as they may be scaled differently although I'm sure doing just one 
	// co-ordinate yields decent enough results) and then multiply it by 0.25 (in reality 0.50 would be more accurate
	// however it makes a 0.1 valued sprite a little larger than we'd want so for ease of use)
	
	gl_PointSize = 0.25 * (projRatio.x+projRatio.y); 
														  
	gl_Position = P * cameraPos;
}