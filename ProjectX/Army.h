#ifndef ARMY_H
#define ARMY_H
#include "StrategicGameObject.h"
#include <vector>


/*
	This class handles 
*/
class Army: public StrategicGameObject
{
public:
	Army(){ }
	Army(const std::string & name,unsigned int x,unsigned int y): StrategicGameObject(name,x,y){}

	void Render() const;

	void HandleMouseClick(const std::pair<unsigned int,unsigned int>& pos);

	//void PathToPoint(const std::pair<unsigned int,unsigned int>& pos) const;
	//bool IsPoint

	void OnTurnEnd();


private:
	std::vector<std::pair<unsigned int,unsigned int>> m_vTilePath;//The path of tiles to the target tile
};

#endif