#ifndef TURN_MANAGER_H
#define TURN_MANAGER_H
#include <vector>



class TurnListener;


/*
	This class handles the ending of a game turn and in accord with the turn listener class to sync all of game objects end turn events
*/
class TurnManager
{
public:
	TurnManager(){ m_TurnNumber = 0; }
	~TurnManager(){ m_vpGameObjects.clear(); }


	//Calling this function advances the turn count and calls all classes who need to know when a turn ends
	void EndTurn();

	void AddObject(TurnListener*obj) 
	{ 
		m_vpGameObjects.push_back(obj); 
	}

	unsigned int GetTurnNumber() const {return m_TurnNumber; }
private:

	//A list of all game objects which throw a end turn event
	std::vector<TurnListener*> m_vpGameObjects; 

	//The number representing the current turn
	unsigned int m_TurnNumber;
};

extern TurnManager * g_pTurnManager;

#endif