#ifndef STRATEGIC_MAP_MANAGER_H
#define STRATEGIC_MAP_MANAGER_H
#include "MapGenerator.h"
#include "WidgetManager.h"
#include "WidgetEventHandler.h"
#include "StrategicModeWidgetEventHandler.h"


class StrategicMapManager
{
public:

	static void createInstance();
	static StrategicMapManager * getInstance();
	static void deleteInstance();

	void draw();
	void update();
	void checkKeyPress();
	void addGenerationGUI();
	void InitMainGUI();
	void generateNewMap(MapType t);

	Map * GetMap() const {return map; }
private:

	static StrategicMapManager * instance;
	StrategicMapManager();
	~StrategicMapManager();

	WidgetManager widgetManager;

	Map * map;

};
#endif