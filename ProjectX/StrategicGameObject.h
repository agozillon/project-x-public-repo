#ifndef STRATEGIC_GAME_OBJECT_H
#define STRATEGIC_GAME_OBJECT_H
#include <string>
#include "TurnManager.h"
#include "TurnListener.h"



/*
	This class represents a game object by extending a number of common interfaces to its subclasses
*/
class StrategicGameObject: public TurnListener
{
public:

	unsigned int GetX() const { return m_X; }
	unsigned int GetY() const { return m_Y; }

	void SetX(unsigned int x) { m_X = x; }
	void SetY(unsigned int y) { m_Y = y; }

	std::string GetName()const{ return m_Name;}


	//This expects the mouse coordinates to be transformed into tile space first
	//Returns whether or not the mouse is over the same tile as this game object
	bool MouseOver(const std::pair<unsigned int,unsigned int>& pos) const { return pos.first == m_X && pos.second == m_Y; }

	//Optional callback to whether or not the mouse is over the object
	virtual void HandleMouseOver(const std::pair<unsigned int,unsigned int>& pos) { }

	//Optional callback upon mouse clicking on the object
	virtual void HandleMouseClick(const std::pair<unsigned int,unsigned int>& pos) { }
	


	virtual void OnTurnEnd() { }
protected:	
	
	StrategicGameObject() {  }
	StrategicGameObject(const std::string & name,unsigned int x,unsigned int y): m_Name(name),m_X(x),m_Y(y) {}

	//Id of the object
	std::string m_Name;

	//The tile coordinates of the object
	unsigned int m_X,m_Y;
};

#endif