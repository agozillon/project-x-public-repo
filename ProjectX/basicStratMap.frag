#version 330

out vec4 colour;
in vec2 UV;

uniform sampler2D textureUnit0;
uniform vec4 tint;



void main()
{


	colour = texture2D(textureUnit0,UV);
	if(colour.x > 0.9 && colour.y > 0.9 && colour.z > 0.9 ) colour.w = 0.0;

	colour = colour*tint;

//	colour = vec4(1.0,0.0,0.0,1.0);
}