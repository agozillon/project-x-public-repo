#ifndef CAMERA_MANAGER_H
#define CAMERA_MANAGER_H
#include <map>
#include <string>
#include "Camera.h"

// CameraManager class its job is similar to the LevelManager it holds all the Cameras
// that are used within the game within a map and allows switching between them using 
// a map key, you can easily add a new camera to the CameraManager. all functions are 
// always called on the current camera.
class CameraManager
{
public:

	static void createInstance();
	static void deleteInstance();
	static CameraManager *getInstance();


	void setActiveCamera(const std::string&name);
	void addCamera(const std::string &name, const glm::vec3&eye, const glm::vec3&target, const glm::vec3&up, const float fov, const float n, const float f);
	void moveCameraRight(float dist);
	void moveCameraUp(float dist);
	void moveCameraForward(float dist);
	void rotateCamera(const float y, const float p);

	glm::mat4 getLookAt();

	void setEye(const glm::vec3& eye);
	void setUp(const glm::vec3& up);
	void setTarget(const glm::vec3& target);

	const inline glm::vec3& getEye() { return cameras[camera]->getEye();}		
	inline float* getProjectionValuePtr() { return cameras[camera]->getProjectionValuePtr();}
	const inline glm::mat4& getProjection() { return cameras[camera]->getProjection(); }
	const inline float getFOV() { return cameras[camera]->getFOV();}
	const inline float getNear() { return cameras[camera]->getNear();}
	const inline float getFar() { return cameras[camera]->getFar();}

private:
	
	std::string camera;
	static CameraManager * instance;

	std::map<std::string,Camera*> cameras;

	CameraManager();
	~CameraManager();
};

#endif