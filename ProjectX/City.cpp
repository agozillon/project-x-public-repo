#include "City.h"
#include "ShaderManager.h"
#include "RenderManager.h"
#include "MapManager.h"


void City::Render() const
{

	glDisable(GL_DEPTH_BUFFER);

	ShaderManager::getInstance()->use("StrategicMap");

	int x = MapManager::getInstance()->GetX();
	int y = MapManager::getInstance()->GetY();
	int zoom = MapManager::getInstance()->GetZoom();
	float left =  x-zoom;
	float right=  x+zoom;	
	float bottom= y-zoom ;
	float top  =  y+zoom;
	glm::mat4 projection = glm::ortho(left,right,bottom,top);
	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("projection",1,false,glm::value_ptr(projection));


	glm::mat4 mvp(1.0f);
	mvp = glm::translate(mvp,glm::vec3(x,y,0.0f));
	ShaderManager::getInstance()->getShader("StrategicMap")->setUniformMatrix4fv("MVP",1,false,glm::value_ptr(mvp));
	RenderManager::getInstance()->renderRenderable("City");
	glEnable(GL_DEPTH_BUFFER);
}
