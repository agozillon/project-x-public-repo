#ifndef SCENERY_H
#define SCENERY_H
#include "WorldObjects.h"
#include <string>
using namespace glm;	

// Scenery class which is essentially used for rendering every WorldObject that isn't 
// a Unit at the moment. It's fairly similar to the Unit class in some aspects
// except that its overall a whole lot simpler. Especially the rendering code.
class Scenery : public WorldObjects
{
public:
	const inline vec3 getPosition(){return position;}     
	const inline vec3 getRotation(){return rotation;}
	const inline vec3 getScalar(){return scalar;}
	const inline bool getUnitCollideable(){return unitCollideable;}
	const inline bool getRayPickable(){return rayPickable;}
	inline void updatePosition(vec3 pos){position = pos;}
	inline void updateRotation(vec3 rot){rotation = rot;}
	inline void updateScalar(vec3 scale){scalar = scale;}
	inline void updateUnitCollideable(bool nCollideable){unitCollideable = nCollideable;}
	inline void updateRayPickable(bool nPickable){rayPickable = nPickable;}
	void draw(mat4 viewProjection);
	inline BoundingBox* getCollisionBox(){return collisionBox;}
	~Scenery();
	Scenery(vec3 pos, vec3 rot, vec3 scale, std::string mesh, std::string texture, std::string shader, bool uCollideable, bool pickable);
	Scenery(vec3 pos, vec3 rot, vec3 scale, vec3 bboxDimensions, std::string mesh, std::string texture, std::string shader, bool uCollideable, bool pickable);
private:
	// variables for the position and orientation of the object in the world
	vec3 scalar;
	vec3 rotation;
	vec3 position;

	// like it says on the tin, a bool saying if it can be picked or not by the mouse!
	// or if a unit can collide with it I.E it's a physical object. Examples of things that
	// aren't collideable are the floor as it can cause some issues with the Pathfinding as
	// all the scenery gets added into one vector. Not collideable makes it easier to distinguish
	// and pass over. 
	bool unitCollideable;
	bool rayPickable; 
	
	BoundingBox * collisionBox;
	std::string meshName;
	std::string textureName;
	std::string shaderName;
};

#endif