#include "FactionManager.h"
#include "TurnManager.h"


FactionManager * FactionManager::instance = nullptr;


FactionManager::FactionManager()
{
	factions.push_back(new Faction("NULL",0,glm::vec4(1.0f,1.0f,1.0f,1.0f)));
	m_pSelectedArmy = 0;
}

FactionManager::~FactionManager()
{}

Faction * FactionManager::getFaction(const std::string &str) const
{
	for(unsigned int i = 1; i < factions.size(); i++)
	{
		if( factions[i]->getName().compare(str) )
			return factions[i];
	}

	return factions[0];
}


Faction* FactionManager::getFaction(const unsigned int index) const
{
	for(unsigned int i = 1; i < factions.size(); i++)
	{
		if( factions[i]->getIndex() == index )
			return factions[i];
	}

	return factions[0];
}

const glm::vec4 & FactionManager::getColour(const std::string &str) const
{
	for(unsigned int i = 1; i < factions.size(); i++)
	{
		if( factions[i]->getName().compare(str) == 0 )
			return factions[i]->getColour();
	}

	return factions[0]->getColour();
}


const glm::vec4 & FactionManager::getColour(const unsigned int index) const
{
	for(unsigned int i = 1; i < factions.size(); i++)
	{
		if( factions[i]->getIndex() == index )
			return factions[i]->getColour();
	}

	return factions[0]->getColour();
}


void FactionManager::add(Faction *f)
{
	factions.push_back(f);
}

FactionManager* FactionManager::getInstance()
{
	if( !instance )createInstance();
	
	return instance;
}

void FactionManager::deleteInstance()
{
	if( !instance ) return;
	delete instance;
	instance = nullptr;

}

void FactionManager::createInstance()
{
	if( instance ) return;
	instance = new FactionManager();
}


void FactionManager::RenderAllFactionObjects() const
{
	for(unsigned int i = 1; i < factions.size(); i++)
	{
		factions[i]->RenderObjects();
	}
}


void FactionManager::CheckClick(const std::pair<unsigned int,unsigned int> & pos)
{
	
	//if( factions[1]->GetArmy("First Army").MouseOver(pos) )
	factions[1]->GetArmy("First Army").HandleMouseClick(pos);
	
}