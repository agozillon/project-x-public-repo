#ifndef CITY_H
#define CITY_H
#include "StrategicGameObject.h"


class City: public StrategicGameObject
{
public:
	City(const std::string &name,unsigned int xPos,unsigned int yPos): StrategicGameObject(name,xPos,yPos) { }

	void Render() const;

};

#endif