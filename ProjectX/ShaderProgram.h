#ifndef SHADER_PROGRAM_H
#define SHADER_PROGRAM_H
#include "Shader.h"
#include <vector>

class ShaderProgram
{
public:
	ShaderProgram(std::string str);
	~ShaderProgram();

	GLuint getID();
	void use();
	void link();
	void attachShader(Shader * shader);

	bool isActive() const { return active; }
	bool isLinked() const{ return linked; }
	void deactivate() { active = false; }

	/*
		Masks for the existing OpenGL calls
	*/
	void setUniform1i(const std::string & paramName,const int i) const;
	void setUniform1f(const std::string & paramName,const float f) const;
	void setUniform2fv(const std::string & paramName, int count, float *f) const;
	void setUniform3fv(const std::string & paramName,int count,float *f) const;
	void setUniform4fv(const std::string & paramName,int count,float *f) const;
	void setUniformMatrix3fv(const std::string & paramName,int count,bool transpose,float *value)const;
	void setUniformMatrix4fv(const std::string & paramName,int count,bool transpose,float *value)const;


private:
	std::vector<Shader*> shaders;
	std::string programName;

	GLuint id;
	bool active;
	bool linked;
	GLuint getUniformLocation(const std::string & paramName) const;

	ShaderProgram(); //so they must have a name
};

#endif