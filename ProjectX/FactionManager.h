#ifndef FACTION_MANAGER_H
#define FACTION_MANAGER_H
#include <vector>
#include "Faction.h"



class FactionManager
{
public:
	static FactionManager * getInstance();
	static void createInstance();
	static void deleteInstance();

	void add(Faction*faction);
	Faction*getFaction(const std::string &str) const;
	Faction*getFaction(const unsigned int index) const;

	const glm::vec4 & getColour(const std::string &str) const;
	const glm::vec4 & getColour(const unsigned int index) const;


	void CheckClick(const std::pair<unsigned int,unsigned int>&);
	void RenderAllFactionObjects() const;

	Army * GetSelectedArmy() const   { return m_pSelectedArmy; }
	void SetSelectedArmy(Army * army){ m_pSelectedArmy = army; }
private:
	FactionManager();
	~FactionManager();
	std::vector<Faction*> factions;


	Army * m_pSelectedArmy;
	static FactionManager * instance;

};

#endif