#include "TacticalMapState.h"
#include "TacticalMapManager.h"
#include "CameraManager.h"

TacticalMapState::TacticalMapState()
{
	TacticalMapManager::createInstance();
}

TacticalMapState::~TacticalMapState()
{
	TacticalMapManager::deleteInstance();
}

// renders the whole tactical map state
void TacticalMapState::draw()
{	
	TacticalMapManager::getInstance()->draw();
}

// updates the whole tactical map manager and checks for input
void TacticalMapState::update()
{
	TacticalMapManager::getInstance()->update();
	checkInput();
}

// function that calls the map managers check for input function
void TacticalMapState::checkInput()
{
	TacticalMapManager::getInstance()->checkKeyPress();
}

// gets called on entry to this state
void TacticalMapState::onEntry()
{
	// sets the active camera to the Tactical Map stops the camera being stuck on strategic
	CameraManager::getInstance()->setActiveCamera("TacticalMap");
}
