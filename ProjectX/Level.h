#ifndef LEVEL_H
#define LEVEL_H
#include "Scenery.h"
#include "BoundingBox.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"
#include <vector>
#include <string>

// Abstract interface of the level class that virtualizes a bunch of functions that every 
//level will use and must create to be instantiated
class Level{
public:
	virtual ~Level(){}
	virtual void init() = 0;
	virtual void draw(glm::mat4 viewProjection) = 0;
	virtual void update() = 0;
	virtual void getScenery(std::vector<Scenery*> &scenery) = 0;
	virtual inline void getDamageEnvironmentalEffects(std::vector<DamageEnvironmentalEffect*> &damageEffects) = 0;
	virtual inline void getSlowEnvironmentalEffects(std::vector<SlowEnvironmentalEffect*> &slowEffects) = 0;
	virtual inline BoundingBox * getPlayerDeploymentZone() = 0;
	virtual inline BoundingBox * getEnemyDeploymentZone() = 0;

};
#endif