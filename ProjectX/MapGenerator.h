#ifndef RANDOM_MAP_GENERATOR_H
#define RANDOM_MAP_GENERATOR_H
#include "Map.h"
#include <vector>

typedef std::pair<int,int> TILE_XY;

//This is passed to the generator to determine the type of map
enum MapType
{
	SMALL_ISLANDS,
	LARGE_ISLANDS,
	CONTINENTS,
	MEDITERRANEAN,
	LANDMASS
};

class MapGenerator
{
public:
	MapGenerator() {} 
	~MapGenerator() {} 

	//Returns a pointer to the newly generated map, with the dimentions of width and height and following the pattern of the type parrameter
	Map * generateMap(const int width,const int height,const int numberOfFactions,MapType type);
private:
	std::vector<float> generateHeightMap(const  int width,const  int height);
    

	//This is used 
	enum Direction
	{
		UP,
		DOWN,
		RIGHT,
		LEFT,
		UPPER_RIGHT,
		UPPER_LEFT,
		BOTTOM_RIGHT,
		BOTTOM_LEFT,
		CENTER //for the seed tile
	};


	//Recursivly spawns tiles around a point 
	void spawnTilesAround(const int x,const int y,const int elevation,Direction directionFrom);

	//Recursivly places factions around a point
	void setFactionFromPoint(const int x,const int y,const  int distance,const  int factionNumber,Direction directionFrom);

	std::vector<std::pair<TILE_XY,int>> getSeedTiles(MapType);	//Semi-randomly select a number of points to be the mountain seeds

	//The vector of vectors of tiles which represents the map
	std::vector<std::vector<Tile>> *tiles;

	std::vector<TILE_XY> landTiles;    //Land provinces 
	std::vector<TILE_XY> factionTiles; //Tiles occupied by a faction (only takes center tile) 

	//Width and height of the map
	int width,height;

	//The number of tiles held by each faction
	int factionDomainSize;
	int seaLevel; // in range 0-1000


	//Removes lone land tiles at sea
	void removeLoners();

};

#endif