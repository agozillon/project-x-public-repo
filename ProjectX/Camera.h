#ifndef CAMERA_H
#define CAMERA_H
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

// Basic 3D perspective projection camera class used in the CameraManager class
// the Camera supports movement and rotation within a 3D environment and can return
// all the various variables that go into creating the camera such as the FOV, near and far
// distance. It uses the GLM libraries functionallity to facilitate this.
class Camera
{
public:
	Camera(const glm::vec3 &eye,const glm::vec3 &target,const glm::vec3&up, const float fov, const float n, const float f);
	~Camera();

	glm::mat4 getLookAt();

	void rotateCamera(const float y, const float p);
	void setEye(const glm::vec3& eye);
	void setUp(const glm::vec3& up);
	void setTarget(const glm::vec3& target);
	void moveCameraRight(float dist);
	void moveCameraUp(float dist);
	void moveCameraForward(float dist);
	
	float* getProjectionValuePtr() {return glm::value_ptr(projection); }
	const glm::mat4 & getProjection() { return projection; }
	const float getFOV(){return FOV;}
	const float getNear(){return near;}
	const float getFar(){return far;}

	const glm::vec3 & getEye(){ return eye; }
	const glm::vec3 & getTarget() { return target; }
	const glm::vec3 & getUp() { return up; }


private:
	glm::vec3 eye, target, up;
	glm::mat4 projection;

	float yaw, pitch, FOV, near, far;

	Camera(){}
};


#endif