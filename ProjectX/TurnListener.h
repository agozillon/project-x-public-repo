#ifndef TURN_LISTENER_H
#define TURN_LISTENER_H

//This class simply extends an interface to the user which in concert with TurnManager will call OnTurnEnd upon the end of every turn
class TurnListener
{
public:

	//This function is called on the end of every turn 
	virtual void OnTurnEnd() = 0;
};

#endif