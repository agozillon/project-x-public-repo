#include "TacticalDeploymentWidgetEventHandler.h"
#include "TacticalMapManager.h"

void TacticalDeploymentWidgetEventHandler::handleMouseLeftClick(Widget *widget)
{
	// simple widget event handler, basically just checks if the AcceptButton widget has been pressed
	// then switches the state and passes the levelManager, playersUnits and EnemyUnits on to the TacticalCombat state
	// the accept button is the button at the top of the screen within the deployment mode
	if( widget->getName().compare("AcceptButton") == 0 && widget->getPreviousTimeStamp() + 50 < widget->getTimeStamp() )
	{
		TacticalMapManager::getInstance()->getCombatMode()->setMap(levelManager, *playerUnits, *enemyUnits);
		TacticalMapManager::getInstance()->changeTacticalMode(COMBAT_MODE);
	}
}
