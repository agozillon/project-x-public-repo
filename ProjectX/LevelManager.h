#ifndef LEVEL_MANAGER_H
#define LEVEL_MANAGER_H
#include "Level.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"
#include <map>
#include <vector>
#include <string>

class Scenery;

// LevelManager class that essentially just manages the currently selected level and stores
// all the existing levels. And allows switching between the set levels!
class LevelManager{
public:
	~LevelManager();
	LevelManager();
	void init();
	void draw(glm::mat4 viewProjection);
	void switchLevel(std::string levelName);
	void update();
	inline void getScenery(std::vector<Scenery*> &scenery){currentLevel->getScenery(scenery);}
	inline void getDamageEnvironmentalEffects(std::vector<DamageEnvironmentalEffect*> &damageEffects){currentLevel->getDamageEnvironmentalEffects(damageEffects);}
	inline void getSlowEnvironmentalEffects(std::vector<SlowEnvironmentalEffect*> &slowEffects){currentLevel->getSlowEnvironmentalEffects(slowEffects);}
	inline BoundingBox * getPlayerDeploymentZone(){return currentLevel->getPlayerDeploymentZone();}
	inline BoundingBox * getEnemyDeploymentZone(){return currentLevel->getEnemyDeploymentZone();}

private:
	Level * currentLevel;
	std::map<std::string, Level*> levelList;

};
#endif