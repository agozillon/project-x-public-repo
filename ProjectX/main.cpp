#include "game.h"

int main(int argc, char * argv[])
{
	Game * gameLoop;
	gameLoop = new Game(800,640);


	while(gameLoop->isRunning() )
		gameLoop->run();

	delete gameLoop;
	return 0;
}