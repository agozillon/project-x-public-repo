#include "DamageEnvironmentalEffect.h"

// just instantiates the collision box and particle effect that the Environmental Effect needs
DamageEnvironmentalEffect::DamageEnvironmentalEffect(const int dmg, const vec3 pos, const vec3 dimensions, const std::string texture, const std::string shader, const int particleCount, const glm::vec2 screenResolution)
	: damage(dmg)
{
	indicator = new Particles(particleCount, dimensions, pos, screenResolution, 0.1f, texture, shader);
	collisionBox = new BoundingBox(pos, dimensions);
}

// calling this on the passed in unit will damage it, via calling its damageUnit function and then passing in 
// its health negated by damage
void DamageEnvironmentalEffect::IncurEnviromentalEffect(Unit * effectedUnit)
{
	effectedUnit->damageUnit(effectedUnit->getHealth()-damage);
}

// basic draw method that calls the particles class draw method, it also updates the particles!
// I decided it would be better to encapsulate away the particles needs for updating rather than
// creating another function call specifically for it.
void DamageEnvironmentalEffect::draw(mat4 view, mat4 projection)
{
	indicator->update();
	indicator->draw(view, projection);
}

DamageEnvironmentalEffect::~DamageEnvironmentalEffect()
{
	delete indicator;
	delete collisionBox;
}
