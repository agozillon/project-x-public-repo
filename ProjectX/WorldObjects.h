#ifndef WORLD_OBJECTS_H
#define WORLD_OBJECTS_H
#include <glm/gtc/type_ptr.hpp> // including glm/gtc/type_ptr so that this class can use vec3s 
#include "BoundingBox.h"
using namespace glm;	

// Abstract interface class for anything that will get rendered as a 3D Object within 
// the Tactical Map Mode, essentially holds a bunch of variables and function that will be used
// in each 3D Object that will be rendered within the Game World. For instance everything in the world
// need to have a position to be placed! a shader to render with etc.  
class WorldObjects
{
public:
	virtual const vec3 getPosition() = 0;
	virtual const vec3 getRotation() = 0;
	virtual const vec3 getScalar() = 0;
	virtual void updatePosition(vec3 pos) = 0;
	virtual void updateRotation(vec3 rot) = 0;
	virtual void updateScalar(vec3 scale) = 0;
	virtual void draw(mat4 viewProjection) = 0;
	virtual const BoundingBox * getCollisionBox() = 0;
	virtual ~WorldObjects(){}

private:
	vec3 scalar;
	vec3 rotation;
	vec3 position;
	std::string meshName;
	std::string textureName;
	std::string shaderName;
	BoundingBox * collisionBox;

};

#endif