#ifndef SLOW_ENVIRONMENTAL_EFFECT_H
#define SLOW_ENVIRONMENTAL_EFFECT_H
#include "Unit.h"
#include "EnvironmentalEffect.h"
#include "Particles.h"
#include <string>
using namespace glm;	

// SlowEnvironmentalEffect essentially just an environmental effect that
// slows units that pass through it by invoking the IncurEnvironmentalEffect
// function, its displayed on screen by a particle effect!
class SlowEnvironmentalEffect : public EnvironmentalEffect
{
public:
	const inline vec3 getPosition(){return position;}
	inline void updatePosition(vec3 pos){position = pos;}
	void draw(mat4 view, mat4 projection);
	inline BoundingBox* getCollisionBox(){return collisionBox;}
	void IncurEnviromentalEffect(Unit * effectedUnit);
	~SlowEnvironmentalEffect();
	SlowEnvironmentalEffect(float slowAmount, const vec3 pos, const vec3 dimensions, const std::string texture, const std::string shader, const int particleCount, const glm::vec2 screenResolution);
private:
	vec3 position; 
	float slowCoefficient;
	BoundingBox * collisionBox;
	Particles * indicator;
};

#endif