#include "Game.h"
#include <iostream>
#include "GameManager.h"
#include <GL\glew.h>
#include <SDL_ttf.h>
#include "Mouse.h"


Game::Game(const unsigned int width,const unsigned int height)
{
	init(width,height);
	glGetError(); // swallow weird 1280 error from init, we should look into it later 

	GameManager::createInstance(width,height);

}
void Game::init(const unsigned int width,const unsigned int height)
{
	running = true;

	SDL_Init(SDL_INIT_VIDEO);

	    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE); 

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);  // double buffering on
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4); // Turn on x4 multisampling anti-aliasing (MSAA)
	//SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8); // creating an alpha buffer

	window = SDL_CreateWindow("Project X",SDL_WINDOWPOS_CENTERED,SDL_WINDOWPOS_CENTERED,width,
		height,SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN );
	context = SDL_GL_CreateContext(window);

	SDL_GL_SetSwapInterval(1);
	glEnable(GL_DEPTH_TEST);
	glClearColor(0.4f,0.4f,0.4f,1.0f);

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);

	glewExperimental = GL_TRUE;
	GLenum err = glewInit();

	if (GLEW_OK != err)
	{
	/* Problem: glewInit failed, something is seriously wrong. */
		fprintf(stderr, "Error: %s\n", glewGetErrorString(err));
	}

	// set up TrueType / SDL_ttf, initializng text font and checking it works before moving on
	if (TTF_Init()== -1)
		std::cout << "TTF failed to initilize";

	// text font pointer to hold the passed in font
	TTF_Font* textFont = TTF_OpenFont("MavenPro-Regular.ttf", 24);

	if (textFont == NULL)
		std::cout << "failed to open font";

	TTF_CloseFont(textFont);

	fprintf(stdout, "Status: Using GLEW %s\n", glewGetString(GLEW_VERSION));
}




void Game::draw()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	GameManager::getInstance()->draw();
	

	SDL_GL_SwapWindow(window);
}

void Game::update()
{
	GameManager::getInstance()->update();
	GameManager::getInstance()->checkState();
}

void Game::run()
{
	SDL_Event e;
	while( SDL_PollEvent(&e) ) 
	{
		if(e.type == SDL_QUIT ) 
			running = false;
		
		if(e.type & 0x400)
			parseMouse(e);
	}

	update();
	draw();
}

void Game::parseMouse(SDL_Event& e)
{
	Mouse::getInstance()->updateEvent(e);
}

Game::~Game()
{
	GameManager::deleteInstance();
}

