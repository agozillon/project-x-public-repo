#ifndef ENEMY_MANAGER_H
#define ENEMY_MANAGER_H
#include <vector>
#include <time.h>
#include "Unit.h"
#include "Pathfinder.h"
#include "DamageEnvironmentalEffect.h"
#include "SlowEnvironmentalEffect.h"

// EnemyManager similar to the PlayerManager except that it controls the Units it holds completely differently.
// It's job is to render and update all of the enemy controlled Units and find specific targets for each Unit
// when its previous target has been killed. 
class EnemyManager {
public:
	EnemyManager(std::vector<Unit*> enemyUnits);
	~EnemyManager();
	void draw(glm::mat4 viewProjection); 
	void update(std::vector<Scenery*> scene, std::vector<Unit*> playerUnits, std::vector<DamageEnvironmentalEffect*> dmgEffect, std::vector<SlowEnvironmentalEffect*> slowEffect);
	void targetSelection(std::vector<Unit*> playerUnits, Unit * currentUnit);
	void getUnits(std::vector<Unit*> &u){u = units;}

private:
	std::vector<Unit*> units; // all the enemies units
	PathFinder * pathSystem; // the pathfinding system for the enemy
	
	clock_t previousTime; // time variables for calculating that 0.75 ms's has passed before each AI and Combat update
	clock_t timePassed;
};
#endif

