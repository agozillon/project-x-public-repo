#include "SlowEnvironmentalEffect.h"

// just instantiates the collision box and particle effect that the Environmental Effect needs
SlowEnvironmentalEffect::SlowEnvironmentalEffect(float slowAmount, const vec3 pos, const vec3 dimensions, const std::string texture, const std::string shader, const int particleCount, const glm::vec2 screenResolution)
	: slowCoefficient(slowAmount)
{
	indicator = new Particles(particleCount, dimensions, pos, screenResolution, 0.1f, texture, shader);
	collisionBox = new BoundingBox(pos, dimensions);
}

// calling this on the passed in unit will slow it, via updating its internal slow coefficent
void SlowEnvironmentalEffect::IncurEnviromentalEffect(Unit * effectedUnit)
{
	effectedUnit->updateSlowCoefficent(slowCoefficient);
}

// basic draw method that calls the particles class draw method, it also updates the particles!
// I decided it would be better to encapsulate away the particles needs for updating rather than
// creating another function call specifically for it.
void SlowEnvironmentalEffect::draw(mat4 view, mat4 projection)
{
	indicator->update();
	indicator->draw(view, projection);
}

SlowEnvironmentalEffect::~SlowEnvironmentalEffect()
{
	delete indicator;
	delete collisionBox;
}
