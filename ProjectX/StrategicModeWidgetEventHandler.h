#ifndef STRATEGIC_MODE_WIDGET_EVENT_HANDLER_H
#define STRATEGIC_MODE_WIDGET_EVENT_HANDLER_H
#include "WidgetEventHandler.h"
#include "Widget.h"



//This class handles the GUI commands for the strategic mode
class StrategicModeWidgetEventHandler: public WidgetEventHandler
{
public: 
	StrategicModeWidgetEventHandler() { }

	//This function simply determines what type of world to generate
	void handleMouseLeftClick(Widget *);
};

#endif